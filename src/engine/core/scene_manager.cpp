#include "scene_manager.hpp"
#include "scene.hpp"
#include "entity.hpp"

namespace ivn
{
    scene_manager::scene_manager()
    {
        m_collector.listen<load_scene_event>([this](auto& event)
        {
            if (m_active_scene)
            {
                m_active_scene->deactivate();
            }

            m_active_scene = event.next_scene;
            m_active_scene->activate();
            
            ebus::get_singleton().notify<scene_loaded_event>({});
        });

        m_collector.listen<unload_scene_event>([this](auto&)
        {
            if (m_active_scene)
            {
                m_active_scene->deactivate();
                m_active_scene = nullptr;
            }
        });

        m_collector.listen<get_active_scene_event>([this](auto& event)
        {
            event.read_scene = m_active_scene;
        });

        m_collector.listen<queue_scene_event>([this](auto& event)
        {
            m_queued_scene = event.next_scene;
        });

        m_collector.listen<load_queued_scene_event>([this](auto&)
        {
            ebus::get_singleton().notify<load_scene_event>({m_queued_scene});
            m_queued_scene = nullptr;
        });
    }
}
