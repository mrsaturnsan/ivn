#ifndef LINE_MESH_HPP
#define LINE_MESH_HPP

#include "mesh.hpp"

#include <glm/glm.hpp>

namespace ivn
{
    struct line
    {
        glm::vec2 start;
        glm::vec2 end;
    };

    class line_mesh : public mesh
    {
    private:
        std::uint32_t m_vao;
        std::uint32_t m_vbo;

    public:
        line_mesh();

        ~line_mesh() override;

        void draw() override;

        void update(const line& l);
    };
}

#endif
