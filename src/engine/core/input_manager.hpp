#ifndef INPUT_MANAGER_HPP
#define INPUT_MANAGER_HPP

#include "ebus.hpp"
#include "utility.hpp"

struct GLFWwindow;

namespace ivn
{
    struct mouse_down_event {};

    struct mouse_up_event {};

    struct get_mouse_position_event
    {
        float& x_pos;
        float& y_pos;
    };

    class input_manager
    {
        NO_CPY_MV_ASGN(input_manager)

    private:
        ebus_collector m_collector{&ebus::get_singleton()};
        GLFWwindow* m_window = nullptr;

    public:
        input_manager();

        void get_mouse_position(float& x_pos, float& y_pos);
    };
}

#endif
