#ifndef SPRITE_MESH_HPP
#define SPRITE_MESH_HPP

#include "utility.hpp"
#include "mesh.hpp"

#include <cstdint>
#include <string_view>

namespace ivn
{
    class sprite_mesh : public mesh
    {
        NO_CPY_MV_ASGN(sprite_mesh)

    private:
        std::uint32_t m_vao;
        std::uint32_t m_vbo;
        std::uint32_t m_ebo;

    public:
        sprite_mesh(const std::string_view name);

        ~sprite_mesh() override;

        void serialize_out(nlohmann::json& data) override;

        void draw() override;
    };
}

#endif
