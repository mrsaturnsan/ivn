#ifndef UTILITY_HPP
#define UTILITY_HPP

#include <string>

#define NO_CPY_MV_ASGN(class_name)\
    class_name(const class_name&) = delete;\
    class_name(class_name&&) = delete;\
    class_name& operator=(const class_name&) = delete;\
    class_name& operator=(class_name&&) = delete;

namespace ivn
{
    std::string get_file_contents(const std::string& path);

#ifdef DEBUG
    template <typename T, typename U>
    T cast(U ptr)
    {
        if (ptr)
        {
            auto result = dynamic_cast<T>(ptr);
            assert(result && "Cast failed!");
            return result;
        }
        else
        {
            return nullptr;
        }
    }
#else
    template <typename T, typename U>
    T cast(U ptr)
    {
        return static_cast<T>(ptr);
    }
#endif
}

#endif