#ifndef SHADER_HPP
#define SHADER_HPP

#include "resource.hpp"
#include "utility.hpp"

#include <glm/glm.hpp>

#include <cstdint>
#include <string>
#include <unordered_map>

namespace ivn
{
    class shader : public resource
    {
        NO_CPY_MV_ASGN(shader)

    private:
        std::unordered_map<std::string, std::uint32_t> m_uniforms;
        std::int32_t m_id;
        
    public:
        shader(const std::string& name, const std::string& vs_glsl, const std::string& fs_glsl);

        ~shader() override;

        void serialize_out(nlohmann::json& data) override;

        void bind() const noexcept;

        void set_int(const std::string& name, int value);

        void set_float(const std::string& name, float value);

        void set_vec2f(const std::string& name, glm::vec2 value);

        void set_vec3f(const std::string& name, glm::vec3 value);

        void set_vec4f(const std::string& name, const glm::vec4& value);

        void set_mat4(const std::string& name, const glm::mat4& matrix);
        
    private:
        void cache_uniforms();
    };
}

#endif
