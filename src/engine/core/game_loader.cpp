#include "game_loader.hpp"
#include "resource_manager.hpp"
#include "texture.hpp"
#include "entity.hpp"
#include "sprite_mesh.hpp"
#include "shader.hpp"
#include "scene.hpp"

#include <fstream>
#include <iostream>

namespace
{
    constexpr const char* SPRITE_MESH_NAME = "sprite_mesh<default>";
    constexpr const char* SPRITE_SHADER_NAME = "sprite_shader<default>";
    constexpr const char* SPRITE_SHADER_VS = "sprite.vert";
    constexpr const char* SPRITE_SHADER_FS = "sprite.frag";
}

namespace ivn
{
    std::size_t convert_to_size_t(const nlohmann::json& str)
    {
        return std::stoull(str.get<std::string>());
    }
    
    game_loader::game_loader()
    {
        m_collector.listen<load_game_from_file_event>([this](auto& event)
        {
            load_game(event.file_name);
        });

        m_collector.listen<get_resource_from_file_id_event>([this](auto& event)
        {
            if (auto it = m_loaded_resources.find(convert_to_size_t(event.value));
                it != m_loaded_resources.end())
            {
                event.out = it->second;
            }
        });

        m_collector.listen<get_resource_from_file_id_raw_event>([this](auto& event)
        {
            if (auto it = m_loaded_resources.find(event.value);
                it != m_loaded_resources.end())
            {
                event.out = it->second;
            }
        });

        instantiate_core_resources();
    }

    void game_loader::load_game(const std::string_view file_name)
    {
        std::ifstream ifs(file_name.data());

        if (!ifs)
        {
            return;
        }

        ebus::get_singleton().notify<resource_clear_event>({});
        m_loaded_resources.clear();
        instantiate_core_resources();

        auto game_data = nlohmann::json::parse(ifs);

        map_core_resources(game_data);
        instantiate_textures(game_data);
        instantiate_entities(game_data);
        instantiate_scripts(game_data);
        instantiate_scenes(game_data);
    }

    void game_loader::instantiate_core_resources()
    {
        auto sm = std::make_unique<sprite_mesh>(SPRITE_MESH_NAME);
        m_mesh = sm.get();
        ebus::get_singleton().notify<resource_add_event>({std::move(sm)});

        auto ss = std::make_unique<shader>(SPRITE_SHADER_NAME, 
            get_file_contents(SPRITE_SHADER_VS), get_file_contents(SPRITE_SHADER_FS));
        m_shader = ss.get();
        ebus::get_singleton().notify<resource_add_event>({std::move(ss)});
    }

    void game_loader::commit_resource(std::size_t id, std::unique_ptr<resource> res)
    {
        m_loaded_resources.emplace(id, res.get());
        ebus::get_singleton().notify<resource_add_event>({std::move(res)});
    }

    void game_loader::map_core_resources(const nlohmann::json& data)
    {
        for (const auto& [key, value] : data["Mesh"].items())
        {
            for (const auto& [mesh_id, name] : value.items())
            {
                m_loaded_resources[convert_to_size_t(mesh_id)] = m_mesh;
            }
        }

        for (const auto& [key, value] : data["Shader"].items())
        {
            for (const auto& [shader_id, name] : value.items())
            {
                m_loaded_resources[convert_to_size_t(shader_id)] = m_shader;
            }
        }
    }

    void game_loader::instantiate_textures(const nlohmann::json& data)
    {
        if (!data.contains("Texture"))
        {
            return;
        }

        for (const auto& [key, value] : data["Texture"].items())
        {
            for (const auto& [id, texture_data] : value.items())
            {
                commit_resource(convert_to_size_t(id), texture::serialize_in(texture_data));
            }
        }
    }

    void game_loader::instantiate_entities(const nlohmann::json& data)
    {
        if (!data.contains("Entity"))
        {
            return;
        }

        for (const auto& [key, value] : data["Entity"].items())
        {
            for (const auto& [entity_id, entity_data] : value.items())
            {
                commit_resource(convert_to_size_t(entity_id), entity::serialize_in(entity_data));
            }
        }
    }

    void game_loader::instantiate_scripts(const nlohmann::json& data)
    {
        if (!data.contains("Script"))
        {
            return;
        }

        for (const auto& [key, value] : data["Script"].items())
        {
            for (const auto& [script_id, script_data] : value.items())
            {
                commit_resource(convert_to_size_t(script_id), script::serialize_in(script_data));
            }
        }
    }

    void game_loader::instantiate_scenes(const nlohmann::json& data)
    {
        if (!data.contains("Scene"))
        {
            return;
        }

        for (const auto& [key, value] : data["Scene"].items())
        {
            for (const auto& [scene_id, scene_data] : value.items())
            {
                commit_resource(convert_to_size_t(scene_id), scene::serialize_in(scene_data));
            }
        }
    }
}
