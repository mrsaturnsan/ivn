#ifndef MESH_HPP
#define MESH_HPP

#include "resource.hpp"

namespace ivn
{
    class mesh : public resource
    {
    public:
        mesh(const std::string& name) noexcept : resource{resource_type::MESH, name} {}

        virtual void draw() = 0;
    };
}

#endif
