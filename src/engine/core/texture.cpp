#include "texture.hpp"

#define GLEW_STATIC
    #include <GL/glew.h>
#ifdef __APPLE__
    #include <OpenGL/gl.h>
#endif
#define STB_IMAGE_IMPLEMENTATION
#include <stb/stb_image.h>

namespace ivn
{
    texture::texture(const std::string& name, const std::string& file_path) 
        : resource{resource_type::TEXTURE, name}, m_file_name{file_path}
    {
        init();

        // load and generate the texture
        std::int32_t width = 0;
        std::int32_t height = 0;
        std::int32_t nrChannels = 0;
        
        stbi_set_flip_vertically_on_load(true);  
        
        if (auto data = stbi_load(file_path.c_str(), &width, &height, &nrChannels, STBI_rgb_alpha))
        {
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
            glGenerateMipmap(GL_TEXTURE_2D);
            stbi_image_free(data);

            m_width = width;
            m_height = height;
        }
        else
        {
            glDeleteTextures(1, &m_id);
            glBindTexture(GL_TEXTURE_2D, 0);
            throw std::runtime_error{"Failed to load texture: " + file_path};
        }

        glBindTexture(GL_TEXTURE_2D, 0);
    }

    texture::~texture() noexcept
    {
        glDeleteTextures(1, &m_id);
    }

    void texture::serialize_out(nlohmann::json& data)
    {
        auto id = std::to_string(resource::get_id());

        // Construction information
        auto info = nlohmann::json::object();
        info[id] = {{"Name", get_name()}, {"File", m_file_name}};

        data["Texture"].emplace_back(std::move(info));
    }

    void texture::bind() const noexcept
    {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, m_id);
    }

    void texture::unbind() const noexcept
    {
        glBindTexture(GL_TEXTURE_2D, 0);
    }

    std::uint32_t texture::get_id() const noexcept
    {
        return m_id;
    }

    std::uint32_t texture::get_width() const noexcept
    {
        return m_width;
    }

    std::uint32_t texture::get_height() const noexcept
    {
        return m_height;
    }

    void texture::init()
    {
        glGenTextures(1, &m_id);

        if (!m_id)
        {
            throw std::runtime_error{"Failed to create texture!"};
        }

        glBindTexture(GL_TEXTURE_2D, m_id);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    }

    std::unique_ptr<texture> texture::serialize_in(const nlohmann::json& data)
    {
        return std::make_unique<texture>(data["Name"], data["File"]);
    }
}
