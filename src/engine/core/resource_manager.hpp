#ifndef RESOURCE_MANAGER_HPP
#define RESOURCE_MANAGER_HPP

#include "resource.hpp"
#include "singleton.hpp"
#include "ebus.hpp"

#include <memory>
#include <string>
#include <map>
#include <vector>
#include <unordered_map>

namespace ivn
{
    // USINGS
    using resource_table = std::multimap<std::string, std::unique_ptr<resource>>;
    using resource_table_cache = std::unordered_map<std::size_t, resource*>;
    /////////////////////////////////////////

    // EVENTS
    /////////////////////////////////////////
    struct resource_manager_created_event
    {
        class resource_manager* manager;
    };

    struct resource_manager_destroyed_event
    {
        class resource_manager* manager;
    };

    struct resource_add_event
    {
        std::unique_ptr<resource>&& resource;
    };

    struct resource_get_event
    {
        const std::string& resource_name;
        std::vector<resource*>& out;
    };

    struct resource_get_by_id_event
    {
        std::size_t id;
        resource*& out;
    };

    struct resource_manager_get_all_resources
    {
        resource_table*& resources;
    };

    struct resource_remove_event
    {
        resource* remove_resource;
    };

    struct resource_removed_event
    {
        resource* removed_resource;
    };

    struct resource_rename_event
    {
        resource* rename_resource;
        const std::string& new_resource_name;
    };

    struct resource_clear_event {};
    /////////////////////////////////////////

    class resource_manager
    {
    private:
        ebus_collector m_collector{&ebus::get_singleton()};
        resource_table m_resources;
        resource_table_cache m_cache;

    public:
        resource_manager();

        ~resource_manager();

        void add_resource(std::unique_ptr<resource> resource);

        std::vector<resource*> get_resource(const std::string& resource_name) const;

        resource* get_resource(std::size_t id) const;

        void remove_resource(resource* res);

        void rename_resource(const resource* res, const std::string& new_name);

        void clear();
    };
}

#endif
