#include "scene.hpp"
#include "entity.hpp"
#include "engine.hpp"

#include <algorithm>
#include <iostream>

namespace ivn
{
    scene::scene(const std::string& name) : resource{resource_type::SCENE, name}
    {
        m_collector.listen<resource_removed_event>([this](auto& event)
        {
            if (event.removed_resource->get_type() == resource_type::ENTITY)
            {
                entity* e = cast<entity*>(event.removed_resource);
                remove_entity(e);
            }
        });

        m_collector.listen<scene_remove_entity_event>([this](auto& event)
        {
            remove_entity(event.target);
        });

        m_collector.listen<scene_set_root_script_event>([this](auto& event)
        {
            if (m_active)
            {
                m_root_script = event.new_script;

                if (m_active_script == nullptr)
                {
                    m_active_script = event.new_script;
                }
            }
        });

        m_collector.listen<scene_set_active_script_event>([this](auto& event)
        {
            if (m_active)
            {
                m_active_script = event.new_script;
            }
        });

        m_collector.listen<scene_execute_script_event>([this](auto&)
        {
            if (m_active && m_active_script)
            {
                m_active_script->execute();
            }
        });

        m_collector.listen<tick_event>([this](auto& event)
        {
            if (m_active)
            {
                std::for_each(m_entities.begin(), m_entities.end(), [&event](auto& e)
                {
                    e->tick(event.dt);
                });
            }
        });
    }

    scene::~scene()
    {
        deactivate();
    }

    void scene::serialize_out(nlohmann::json& data)
    {
        if (m_active)
        {
            update_scene_state();
        }

        // Insert into ID map
        auto id = std::to_string(get_id());

        auto info = nlohmann::json::object();
        info[id]["Name"] = get_name();
        info[id]["State"] = m_scene_state;

        data["Scene"].emplace_back(std::move(info));
    }

    void scene::add_entity(entity* e)
    {
        assert(e->get_type() == resource_type::ENTITY && 
            "Only entities can be directly added to scenes!");
            
        m_entities.emplace_back(e);
    }
    
    void scene::remove_entity(entity* e)
    {
        auto it = std::find_if(m_entities.begin(), m_entities.end(), [e](const auto& i)
        {
            return i == e;
        });

        if (it != m_entities.end())
        {
            remove_entity_from_state(*it);

            m_entities.erase(it);
        }
    }

    bool scene::contains_entity(const entity* e) const noexcept
    {
        return std::find_if(m_entities.begin(), m_entities.end(), [e](const auto& i)
        {
            return i == e;
        }) != m_entities.end();
    }

    std::vector<entity*>& scene::get_entity_list() noexcept
    {
        return m_entities;
    }

    void scene::activate()
    {
        m_active = true;
        restore_scene_state();
    }

    void scene::deactivate()
    {
        m_active = false;
        update_scene_state();
    }

    bool scene::is_active() const noexcept
    {
        return m_active;
    }

    bool scene::has_entity(const entity* e) const noexcept
    {
        return std::find_if(m_entities.begin(), m_entities.end(), [e](const auto& i)
        {
            return i == e;
        }) != m_entities.end();
    }

    script* scene::get_active_script() const noexcept
    {
        return m_active_script;
    }

    void scene::update_scene_state()
    {
        m_scene_state = nlohmann::json::object();

        std::for_each(m_entities.begin(), m_entities.end(), [this](auto& e)
        {
            auto info = nlohmann::json::object();
            auto& transform_data = info[std::to_string(e->get_id())]["Transform"];

            glm::vec3 pos = e->m_transform.get_position();
            glm::vec2 scale = e->m_transform.get_scale();
            float rotation = e->m_transform.get_rotation();

            transform_data["Position"] = nlohmann::json::array({pos.x, pos.y, pos.z});
            transform_data["Scale"] = nlohmann::json::array({scale.x, scale.y});
            transform_data["Rotation"] = rotation;

            info[std::to_string(e->get_id())]["Text"] = e->m_text.m_text;

            m_scene_state["Entity"].emplace_back(std::move(info));
        });

        if (m_root_script)
        {
            m_scene_state["Script"]["ID"]["Root"] = m_root_script->get_id();

            if (m_active_script)
            {
                m_scene_state["Script"]["ID"]["Active"] = m_active_script->get_id();
            }
        }
    }

    void scene::restore_scene_state()
    {
        if (m_scene_state.contains("Entity"))
        {
            for (const auto& [key, value] : m_scene_state["Entity"].items())
            {
                for (const auto& [entity_id, entity_data] : value.items())
                {
                    resource* res = nullptr;
                    ebus::get_singleton().notify<resource_get_by_id_event>({convert_to_size_t(entity_id), res});

                    entity* e = cast<entity*>(res);

                    if (e)
                    {
                        const auto& pos = entity_data["Transform"]["Position"];
                        const auto& rot = entity_data["Transform"]["Rotation"];
                        const auto& scl = entity_data["Transform"]["Scale"];

                        e->m_transform.set_position(glm::vec3{pos[0].get<float>(), 
                            pos[1].get<float>(), pos[2].get<float>()});
                        e->m_transform.set_rotation(rot.get<float>());
                        e->m_transform.set_scale(glm::vec2{scl[0].get<float>(), 
                            scl[1].get<float>()});

                        e->m_text.m_text = entity_data["Text"];
                    }
                }
            }
        }
    }

    void scene::remove_entity_from_state(const entity* e)
    {
        auto e_sec = m_scene_state.find("Entity");

        if (e_sec != m_scene_state.end())
        {
            auto e_obj = e_sec->find(std::to_string(e->get_id()));

            if (e_obj != e_sec->end())
            {
                e_sec->erase(e_obj);
            }
        }
    }

    std::unique_ptr<scene> scene::serialize_in(const nlohmann::json& data)
    {
        auto new_scene = std::make_unique<scene>(data["Name"]);

        if (data.contains("State"))
        {
            if (data["State"].contains("Entity"))
            {
                const auto& data_e_section = data["State"]["Entity"];
                auto& e_section = new_scene->m_scene_state["Entity"];

                for (const auto& [key, value] : data_e_section.items())
                {
                    for (const auto& [entity_id, entity_data] : value.items())
                    {
                        resource* res = nullptr;
                        ebus::get_singleton().notify<get_resource_from_file_id_event>({entity_id, res});

                        entity* e = cast<entity*>(res);

                        if (e)
                        {
                            new_scene->add_entity(e);

                            // IDs are NOT guaranteed to be the same across multiple runs.
                            auto info = nlohmann::json::object();
                            info[std::to_string(e->get_id())]["Transform"] = entity_data["Transform"];
                            info[std::to_string(e->get_id())]["Text"] = entity_data["Text"];

                            e_section.emplace_back(std::move(info));
                        }
                    }
                }
            }

            if (data["State"].contains("Script"))
            {
                const auto& data_s_section = data["State"]["Script"];
                auto& s_section = new_scene->m_scene_state["Script"];

                // Handle root script
                {
                    resource* res = nullptr;
                    ebus::get_singleton().notify<get_resource_from_file_id_raw_event>({data_s_section["ID"]["Root"].get<std::size_t>(), res});

                    script* scr = cast<script*>(res);

                    if (scr)
                    {
                        new_scene->m_root_script = scr;
                        s_section["ID"]["Root"] = scr->get_id();
                    }
                }

                // Handle active script
                if (data_s_section["ID"].contains("Active"))
                {
                    resource* res = nullptr;
                    ebus::get_singleton().notify<get_resource_from_file_id_raw_event>({data_s_section["ID"]["Active"].get<std::size_t>(), res});

                    script* scr = cast<script*>(res);

                    if (scr)
                    {
                        new_scene->m_active_script = scr;
                        s_section["ID"]["Active"] = scr->get_id();
                    }
                }
            }
        }

        return new_scene;
    }
}
