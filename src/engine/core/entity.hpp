#ifndef ENTITY_HPP
#define ENTITY_HPP

#include "utility.hpp"
#include "../components/transform.hpp"
#include "../components/aabb.hpp"
#include "../components/rendercomp.hpp"
#include "../components/text.hpp"
#include "resource.hpp"
#include "ebus.hpp"

#include <vector>
#include <memory>
#include <unordered_map>

namespace ivn
{
    using resource_list = std::vector<resource*>;

    class entity : public resource
    {
        NO_CPY_MV_ASGN(entity)

    public:
        transform m_transform;
        aabb m_aabb;
        text m_text;

    private:
        ebus_collector m_collector{&ebus::get_singleton()};      
        rendercomp m_rc;
        resource_list m_resources;

    public:
        entity(const std::string& name);

        void serialize_out(nlohmann::json& data) override;

        void add_resource(resource* res);

        void remove_resource(const resource* res);

        bool has_resource(const resource* res) const;

        bool has_resource(resource_type type) const;

        resource_list& get_resources() noexcept;

        void tick(float dt);

    private:
        void handle_resource_add(resource* res);

        void handle_resource_remove(const resource* res);

    public:
        static std::unique_ptr<entity> serialize_in(const nlohmann::json& data);
    };
}

#endif
