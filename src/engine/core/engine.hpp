#ifndef ENGINE_HPP
#define ENGINE_HPP

#include "utility.hpp"
#include "renderer.hpp"
#include "constants.hpp"
#include "resource_manager.hpp"
#include "scene_manager.hpp"
#include "input_manager.hpp"
#include "debug_draw.hpp"
#include "game_loader.hpp"
#include "scripting.hpp"

namespace ivn
{
    struct tick_event
    {
        float dt = 0.0f;
    };

    class engine
    {
        NO_CPY_MV_ASGN(engine)

    private:
        input_manager m_input_manager;
        renderer m_renderer{constants::window_width, constants::window_height};
        resource_manager m_resource_manager;
        scene_manager m_scene_manager;
        debug_drawer m_debug_drawer;
        game_loader m_loader;
        scripting m_script_runner;

    public:
        engine();

        void execute();
    };
}

#endif
