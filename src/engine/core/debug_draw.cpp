#include "debug_draw.hpp"
#include "renderer.hpp"

namespace ivn
{
    debug_drawer::debug_drawer() : m_line_shader{"line_shader",
                                                 get_file_contents("line.vert"), 
                                                 get_file_contents("line.frag")}
    {
        m_collector.listen<draw_line_event>([this](auto& event)
        {
            draw_line(event.l, event.color);
        });

        m_collector.listen<draw_aabb_event>([this](auto& event)
        {
            draw_bounding_box(event.box, event.color);
        });
    }

    debug_drawer::~debug_drawer()
    {

    }

    void debug_drawer::draw_line(const line& l, const glm::vec4& color)
    {
        m_line_shader.bind();

        glm::mat4* proj = nullptr;
        ebus::get_singleton().notify<get_projection_matrix_event>({proj});

        assert(proj && "Something went wrong with the projection matrix!");

        m_line_shader.set_mat4("proj", *proj);
        m_line_shader.set_vec4f("color", color);

        m_mesh.update(l);
        m_mesh.draw();
    }

    void debug_drawer::draw_bounding_box(const aabb& area, const glm::vec4& color)
    {
        m_line_shader.bind();

        glm::mat4* proj = nullptr;
        ebus::get_singleton().notify<get_projection_matrix_event>({proj});

        assert(proj && "Something went wrong with the projection matrix!");

        m_line_shader.set_mat4("proj", *proj);
        m_line_shader.set_vec4f("color", color);

        auto [x_min, y_min, z_min, x_max, y_max, z_max] = area.get_bounds();

        line top;
        line right;
        line bottom;
        line left;

        top.start = glm::vec2{x_min, y_max};
        top.end = glm::vec2{x_max, y_max};

        right.start = glm::vec2{x_max, y_max};
        right.end = glm::vec2{x_max, y_min};

        bottom.start = glm::vec2{x_min, y_min};
        bottom.end = glm::vec2{x_max, y_min};

        left.start = glm::vec2{x_min, y_max};
        left.end = glm::vec2{x_min, y_min};

        m_mesh.update(top);
        m_mesh.draw();

        m_mesh.update(right);
        m_mesh.draw();

        m_mesh.update(bottom);
        m_mesh.draw();

        m_mesh.update(left);
        m_mesh.draw();
    }
}
