#ifndef GAME_LOADER_HPP
#define GAME_LOADER_HPP

#include "ebus.hpp"
#include "resource.hpp"
#include "utility.hpp"

#include <string>
#include <string_view>
#include <unordered_map>

namespace ivn
{
    struct load_game_from_file_event
    {
        std::string_view file_name;
    };

    struct get_resource_from_file_id_event
    {
        const nlohmann::json& value;
        resource*& out;
    };

    struct get_resource_from_file_id_raw_event
    {
        std::size_t value;
        resource*& out;
    };

    std::size_t convert_to_size_t(const nlohmann::json& str);

    class game_loader
    {
        NO_CPY_MV_ASGN(game_loader)
    
    private:
        ebus_collector m_collector{&ebus::get_singleton()};
        std::unordered_map<std::size_t, resource*> m_loaded_resources;
        resource* m_mesh = nullptr;
        resource* m_shader = nullptr;

    public:
        game_loader();

        void load_game(const std::string_view file_name);

    private:
        void instantiate_core_resources();

        void commit_resource(std::size_t id, std::unique_ptr<resource> res);

        void map_core_resources(const nlohmann::json& data);

        void instantiate_textures(const nlohmann::json& data);

        void instantiate_entities(const nlohmann::json& data);

        void instantiate_scripts(const nlohmann::json& data);

        void instantiate_scenes(const nlohmann::json& data);
    };
}

#endif
