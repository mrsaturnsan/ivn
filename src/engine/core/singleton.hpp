#ifndef SINGLETON_HPP
#define SINGLETON_HPP

namespace ivn
{
    template <typename T>
    class singleton
    {
    public:
        static T& get_singleton() noexcept
        {
            static T instance;
            return instance;
        }
    
    protected:
        singleton() = default;
    };
}

#endif
