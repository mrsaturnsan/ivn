#ifndef CONSTANTS_HPP
#define CONSTANTS_HPP

#include <cstdint>

namespace ivn::constants
{
    static constexpr std::int32_t window_width = 600;
    static constexpr std::int32_t window_height = 400;
    
    static constexpr std::int32_t world_size = 100;
}

#endif
