#include "resource_manager.hpp"
#include "scene_manager.hpp"

namespace ivn
{
    resource_manager::resource_manager()
    {
        m_collector.listen<resource_add_event>([this](auto& event)
        {
            add_resource(std::move(event.resource));
        });

        m_collector.listen<resource_get_event>([this](auto& event)
        {
            event.out = get_resource(event.resource_name);
        });

        m_collector.listen<resource_get_by_id_event>([this](auto& event)
        {
            event.out = get_resource(event.id);
        });

        m_collector.listen<resource_manager_get_all_resources>([this](auto& event)
        {
            event.resources = &m_resources;
        });

        m_collector.listen<resource_remove_event>([this](auto& event)
        {
            remove_resource(event.remove_resource);
        });

        m_collector.listen<resource_clear_event>([this](auto&)
        {
            clear();
        });

        m_collector.listen<resource_rename_event>([this](auto& event)
        {
            rename_resource(event.rename_resource, event.new_resource_name);
        });

        ebus::get_singleton().notify<resource_manager_created_event>({this});
    }

    resource_manager::~resource_manager()
    {
        ebus::get_singleton().notify<resource_manager_destroyed_event>({this});
    }

    void resource_manager::add_resource(std::unique_ptr<resource> resource)
    {
        const auto& name = resource->get_name();
        m_cache.emplace(resource->get_id(), resource.get());
        m_resources.emplace(name, std::move(resource));
    }

    std::vector<resource*> resource_manager::get_resource(const std::string& resource_name) const
    {
        std::vector<resource*> matches;
        auto range = m_resources.equal_range(resource_name);

        for (auto it = range.first; it != range.second; ++it)
        {
            matches.emplace_back(it->second.get());
        }

        return matches;
    }

    resource* resource_manager::get_resource(std::size_t id) const
    {
        if (auto it = m_cache.find(id); it != m_cache.end())
        {
            return it->second;
        }
        else
        {
            return nullptr;
        }
    }

    void resource_manager::remove_resource(resource* res)
    {
        auto range = m_resources.equal_range(res->get_name());

        auto it = std::find_if(range.first, range.second, [res](auto& i)
        {
            return i.second.get() == res;
        });

        if (it != range.second)
        {
            ebus::get_singleton().notify<resource_removed_event>({it->second.get()});
            m_cache.erase(res->get_id());
            m_resources.erase(it);
        }
    }

    void resource_manager::rename_resource(const resource* res, const std::string& new_name)
    {
        auto range = m_resources.equal_range(res->get_name());

        auto it = std::find_if(range.first, range.second, [res](auto& i)
        {
            return i.second.get() == res;
        });

        if (it != range.second)
        {
            auto temp = std::move(it->second);
            m_resources.erase(it);
            temp->set_name(new_name);
            m_resources.emplace(new_name, std::move(temp));
        }
    }
    
    void resource_manager::clear()
    {
        for (auto& [name, res] : m_resources)
        {
            ebus::get_singleton().notify<resource_removed_event>({res.get()});
        }

        ebus::get_singleton().notify<unload_scene_event>({});

        m_cache.clear();
        m_resources.clear();
    }
}
