#include "shader.hpp"

#define GLEW_STATIC
    #include <GL/glew.h>
#ifdef __APPLE__
    #include <OpenGL/gl.h>
#endif
#include <glm/gtc/type_ptr.hpp>

#include <array>
#include <iostream>

namespace
{
    auto create_shader(GLenum stage, const char* source)
    {
        auto shader_id = glCreateShader(stage);

        glShaderSource(shader_id, 1, &source, nullptr);
        glCompileShader(shader_id);

        GLint success;
        std::array<char, 512> info_log;
        glGetShaderiv(shader_id, GL_COMPILE_STATUS, &success);

        if (!success)
        {
            glGetShaderInfoLog(shader_id, info_log.size(), nullptr, info_log.data());
            std::cerr << "ERROR: Shader compilation failed - " << info_log.data() << '\n';
        }

        return shader_id;
    }

    auto create_program(const std::string& vs_glsl, const std::string& fs_glsl)
    {
        GLint vs = create_shader(GL_VERTEX_SHADER, vs_glsl.c_str());
        GLint fs = create_shader(GL_FRAGMENT_SHADER, fs_glsl.c_str());

        auto program = glCreateProgram();

        glAttachShader(program, vs);
        glAttachShader(program, fs);
        glLinkProgram(program);

        GLint success;
        std::array<char, 512> info_log;
        glGetProgramiv(program, GL_LINK_STATUS, &success);

        if (!success)
        {
            glGetProgramInfoLog(program, info_log.size(), nullptr, info_log.data());
            std::cerr << "ERROR: Shader linking failed - " << info_log.data() << '\n';
        }

        glDeleteShader(vs);
        glDeleteShader(fs);

        return program;
    }
}

namespace ivn
{
    shader::shader(const std::string& name, const std::string& vs_glsl, const std::string& fs_glsl) 
        : resource{resource_type::SHADER, name}
    {
        m_id = create_program(vs_glsl, fs_glsl);
        cache_uniforms();
    }

    shader::~shader()
    {
        glDeleteProgram(m_id);
    }

    void shader::serialize_out(nlohmann::json& data)
    {
        auto id = std::to_string(get_id());

        auto info = nlohmann::json::object();
        info[id]["Name"] = get_name();

        data["Shader"].emplace_back(std::move(info));
    }

    void shader::bind() const noexcept
    {
        glUseProgram(m_id);
    }

    void shader::set_int(const std::string& name, int value)
    {
        glUniform1i(m_uniforms[name], value);
    }

    void shader::set_float(const std::string& name, float value)
    {
        glUniform1f(m_uniforms[name], value);
    }

    void shader::set_vec2f(const std::string& name, glm::vec2 value)
    {
        glUniform2f(m_uniforms[name], value.x, value.y);
    }

    void shader::set_vec3f(const std::string& name, glm::vec3 value)
    {
        glUniform3f(m_uniforms[name], value.x, value.y, value.z);
    }

    void shader::set_vec4f(const std::string& name, const glm::vec4& value)
    {
        glUniform4f(m_uniforms[name], value.x, value.y, value.z, value.w);
    }

    void shader::set_mat4(const std::string& name, const glm::mat4& matrix)
    {
        glUniformMatrix4fv(m_uniforms[name], 1, GL_FALSE, glm::value_ptr(matrix));
    }

    void shader::cache_uniforms()
    {
        std::int32_t size = 0;
        GLenum type;
        std::array<char, 64> name;
        GLsizei name_length = 0;
        std::int32_t count = 0;

        glGetProgramiv(m_id, GL_ACTIVE_UNIFORMS, &count);

        for (std::int32_t i = 0; i < count; i++)
        {
            glGetActiveUniform(m_id, i, name.size(), &name_length, &size, &type, name.data());
            m_uniforms.emplace(name.data(), glGetUniformLocation(m_id, name.data()));
        }
    }
}
