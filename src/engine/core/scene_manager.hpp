#ifndef SCENE_MANAGER_HPP
#define SCENE_MANAGER_HPP

#include "ebus.hpp"
#include "scene.hpp"

#include <vector>
#include <memory>

namespace ivn
{
    struct load_scene_event
    {
        scene* next_scene = nullptr;
    };

    struct unload_scene_event {};

    struct scene_loaded_event {};

    struct get_active_scene_event
    {
        scene*& read_scene;
    };

    struct queue_scene_event
    {
        scene* next_scene = nullptr;
    };

    struct load_queued_scene_event {};

    class scene_manager
    {
    private:
        ebus_collector m_collector{&ebus::get_singleton()};
        scene* m_active_scene = nullptr;
        scene* m_queued_scene = nullptr;

    public:
        scene_manager();
    };
}

#endif
