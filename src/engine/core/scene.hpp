#ifndef SCENE_HPP
#define SCENE_HPP

#include "utility.hpp"
#include "resource.hpp"
#include "ebus.hpp"
#include "entity.hpp"
#include "script.hpp"

#include <memory>
#include <vector>

namespace ivn
{
    struct scene_remove_entity_event
    {
        entity* target;
    };

    struct scene_set_root_script_event
    {
        script* new_script;
    };

    struct scene_set_active_script_event
    {
        script* new_script;
    };

    struct scene_execute_script_event {};

    class scene : public resource
    {
        NO_CPY_MV_ASGN(scene)

    private:
        ebus_collector m_collector{&ebus::get_singleton()};
        nlohmann::json m_scene_state;
        std::vector<entity*> m_entities;
        script* m_root_script = nullptr;
        script* m_active_script = nullptr;
        bool m_active = false;

    public:
        scene(const std::string& name);

        ~scene() override;

        void serialize_out(nlohmann::json& data) override;

        void add_entity(entity* e);

        void remove_entity(entity* e);

        bool contains_entity(const entity* e) const noexcept;

        std::vector<entity*>& get_entity_list() noexcept;

        void activate();

        void deactivate();

        bool is_active() const noexcept;

        bool has_entity(const entity* e) const noexcept;

        script* get_active_script() const noexcept;

    private:
        void update_scene_state();

        void restore_scene_state();

        void remove_entity_from_state(const entity* e);

    public:
        static std::unique_ptr<scene> serialize_in(const nlohmann::json& data);
    };
}

#endif
