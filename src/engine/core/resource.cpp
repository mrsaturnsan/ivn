#include "resource.hpp"

namespace ivn
{
    resource::resource(resource_type type, const std::string& name) : m_type{type}, 
                                                                      m_name{name}
    {
        static std::size_t uuid = 0;
        m_id = uuid++;
    }

    void resource::set_name(const std::string& name)
    {
        m_name = name;
    }

    const std::string& resource::get_name() const noexcept
    {
        return m_name;
    }

    resource_type resource::get_type() const noexcept
    {
        return m_type;
    }

    std::size_t resource::get_id() const noexcept
    {
        return m_id;
    }

    void resource::serialize_out([[maybe_unused]] nlohmann::json& data)
    {

    }
}
