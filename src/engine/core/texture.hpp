#ifndef TEXTURE_HPP
#define TEXTURE_HPP

#include "resource.hpp"
#include "utility.hpp"

#include <cstdint>
#include <string>
#include <vector>
#include <memory>

namespace ivn
{
    class texture : public resource
    {
        NO_CPY_MV_ASGN(texture)

    private:
        std::string m_file_name;
        std::uint32_t m_id;
        std::uint32_t m_width;
        std::uint32_t m_height;

    public:
        texture(const std::string& name, const std::string& file_path);

        ~texture() noexcept;

        void serialize_out(nlohmann::json& data) override;

        void bind() const noexcept;

        void unbind() const noexcept;

        std::uint32_t get_id() const noexcept;

        std::uint32_t get_width() const noexcept;

        std::uint32_t get_height() const noexcept;
    
    private:
        void init();

    public:
        static std::unique_ptr<texture> serialize_in(const nlohmann::json& data);
    };
}

#endif
