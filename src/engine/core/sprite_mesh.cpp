#include "sprite_mesh.hpp"

#define GLEW_STATIC
    #include <GL/glew.h>
#ifdef __APPLE__
    #include <OpenGL/gl.h>
#endif

#include <array>

namespace ivn
{
    sprite_mesh::sprite_mesh(const std::string_view name) : mesh{name.data()}
    {
        constexpr std::array<float, 20> vertices
        {
            1.0f,  1.0f, 0.0f, 1.0f, 1.0f,
            -1.0f,  1.0f, 0.0f, 0.0f, 1.0f,
            -1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
            1.0f, -1.0f, 0.0f, 1.0f, 0.0f
        };

        constexpr std::array<std::uint16_t, 6> indices
        {
            0, 1, 2,
            0, 2, 3
        };

        glGenVertexArrays(1, &m_vao);
        glGenBuffers(1, &m_vbo);
        glGenBuffers(1, &m_ebo);

        glBindVertexArray(m_vao);

        glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices.data(), GL_STATIC_DRAW);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ebo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices.data(), GL_STATIC_DRAW);

        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), nullptr);
        glEnableVertexAttribArray(0);

        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), 
            reinterpret_cast<void*>(3 * sizeof(float)));
        glEnableVertexAttribArray(1);

        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glBindVertexArray(0);
    }

    sprite_mesh::~sprite_mesh()
    {
        glDeleteVertexArrays(1, &m_vao);
        glDeleteBuffers(1, &m_vbo);
        glDeleteBuffers(1, &m_ebo);
    }

    void sprite_mesh::serialize_out(nlohmann::json& data)
    {
        auto id = std::to_string(get_id());

        // Construction information
        auto info = nlohmann::json::object();
        info[id]["Name"] = get_name();

        data["Mesh"].emplace_back(std::move(info));
    }

    void sprite_mesh::draw()
    {
        glBindVertexArray(m_vao);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, 0);
    }
}
