#include "utility.hpp"

#include <fstream>
#include <streambuf>

namespace ivn
{
    std::string get_file_contents(const std::string& path)
    {
        if (std::ifstream buf{path})
        {
            return {std::istreambuf_iterator<char>(buf), 
                std::istreambuf_iterator<char>()};
        }
        else
        {
            throw std::runtime_error{"Unable to open file: " + path};
        }  
    }
}
