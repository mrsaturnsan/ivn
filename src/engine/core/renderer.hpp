#ifndef RENDERER_HPP
#define RENDERER_HPP

#include "utility.hpp"
#include "ebus.hpp"

#include <glm/glm.hpp>

#include <cstdint>
#include <array>

struct GLFWwindow;

namespace ivn
{
    // EVENTS
    /////////////////////////////////////////
    struct window_created_event
    {
        GLFWwindow* window;
    };

    struct window_destroyed_event {};

    struct window_resized_event
    {
        std::int32_t width;
        std::int32_t height;
    };

    struct get_window_size_event
    {
        std::int32_t& width;
        std::int32_t& height;
    };

    struct get_projection_matrix_event
    {
        glm::mat4*& projection;
    };

    struct get_inverse_position_event
    {
        float& x_pos;
        float& y_pos;
    };

    struct set_clear_color_event
    {
        const std::array<float, 4>& color;
    };

    struct begin_frame_event {};

    struct end_frame_event {};
    /////////////////////////////////////////

    class renderer
    {
        NO_CPY_MV_ASGN(renderer)

    private:
        static constexpr std::array<float, 4> default_clear{0.1f, 0.1f, 0.1f, 1.0f};
        
        ebus_collector m_collector{&ebus::get_singleton()};
        GLFWwindow* m_window = nullptr;
        glm::mat4 m_projection;
        glm::mat4 m_inv_projection;
        std::int32_t m_width = 0;
        std::int32_t m_height = 0;

    public:
        renderer(std::int32_t width, std::int32_t height);

        ~renderer() noexcept;

        GLFWwindow* get_window() const noexcept;

        const glm::mat4& get_projection() const noexcept;

        void update_viewport(std::int32_t width, std::int32_t height) noexcept;

        bool should_close() const noexcept;

        void begin_frame();

        void end_frame();
    
    private:
        void setup_render_state();
    };
}

#endif
