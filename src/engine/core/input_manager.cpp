#include "input_manager.hpp"
#include "renderer.hpp"

#include <GLFW/glfw3.h>

namespace ivn
{
    input_manager::input_manager()
    {
        m_collector.listen<window_created_event>([this](auto& event)
        {
            m_window = event.window;

            glfwSetMouseButtonCallback(m_window, []([[maybe_unused]] GLFWwindow* window, 
                auto button, auto action, [[maybe_unused]] auto mods)
            {
                if (button == GLFW_MOUSE_BUTTON_LEFT)
                {
                    switch (action)
                    {
                        case GLFW_PRESS:
                            ebus::get_singleton().notify<mouse_down_event>({});
                            break;
                        
                        case GLFW_RELEASE:
                            ebus::get_singleton().notify<mouse_up_event>({});
                            break;
                        
                        default:
                            break;
                    };
                }
            });
        });

        m_collector.listen<get_mouse_position_event>([this](auto& event)
        {
            get_mouse_position(event.x_pos, event.y_pos);
        });
    }

    void input_manager::get_mouse_position(float& x_pos, float& y_pos)
    {
        if (m_window)
        {
            // Convert mouse position to NDC
            std::int32_t x_size = 0;
            std::int32_t y_size = 0;
            ebus::get_singleton().notify<get_window_size_event>({x_size, y_size});

            double x_win_pos = 0.0;
            double y_win_pos = 0.0;
            glfwGetCursorPos(m_window, &x_win_pos, &y_win_pos);

            x_pos = 2.0f * (static_cast<float>((x_win_pos * 2.0) / x_size) - 0.5f);
            y_pos = 2.0f * (0.5f - static_cast<float>((y_win_pos * 2.0) / y_size));

            // Multiply by inverse projection
            ebus::get_singleton().notify<get_inverse_position_event>({x_pos, y_pos});
        }
    }
}
