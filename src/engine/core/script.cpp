#include "script.hpp"
#include "scripting.hpp"

namespace ivn
{
    script::script(const std::string& name) : resource{resource_type::SCRIPT, name}
    {

    }

    void script::execute()
    {
        if (m_instruction_pointer < m_instructions.size())
        {
            ebus::get_singleton().notify<execute_script_event>({m_instructions[m_instruction_pointer]});
        }
    }

    void script::serialize_out(nlohmann::json& data)
    {
        auto id = std::to_string(get_id());

        auto instructions = nlohmann::json::array();

        for (const auto& i : m_instructions)
        {
            instructions.emplace_back(i);
        }

        auto info = nlohmann::json::object();
        auto& details = info[id];
        details["Name"] = get_name();
        details["Instructions"] = std::move(instructions);

        data["Script"].emplace_back(std::move(info));
    }

    std::vector<std::string>& script::get_instructions() noexcept
    {
        return m_instructions;
    }

    std::size_t script::get_instruction_pointer() const noexcept
    {
        return m_instruction_pointer;
    }

    void script::set_instruction_pointer(std::size_t instruction_pointer) noexcept
    {
        if (instruction_pointer < m_instructions.size())
        {
            m_instruction_pointer = instruction_pointer;
        }
    }

    std::unique_ptr<script> script::serialize_in(const nlohmann::json& data)
    {
        auto new_script = std::make_unique<script>(data["Name"]);

        for (const auto& ins : data["Instructions"])
        {
            new_script->m_instructions.emplace_back(ins);
        }

        return new_script;
    }
}
