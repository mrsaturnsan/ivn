#include "line_mesh.hpp"

#define GLEW_STATIC
#include <GL/glew.h>
#ifdef __APPLE__
    #include <OpenGL/gl.h>
#endif

#include <glm/gtc/type_ptr.hpp>

#include <array>

namespace ivn
{
    line_mesh::line_mesh() : mesh{"line_mesh"}
    {
        constexpr std::array<float, 4> line_points
        {
            -1.0f, 0.0f,
            1.0f, 0.0f
        };

        glGenVertexArrays(1, &m_vao);
        glGenBuffers(1, &m_vbo);

        glBindVertexArray(m_vao);

        glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
        glBufferData(GL_ARRAY_BUFFER, sizeof(line_points), line_points.data(), GL_DYNAMIC_DRAW);

        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
        glEnableVertexAttribArray(0);

        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glBindVertexArray(0);
    }

    line_mesh::~line_mesh()
    {
        glDeleteVertexArrays(1, &m_vao);
        glDeleteBuffers(1, &m_vbo);
    }

    void line_mesh::draw()
    {
        glBindVertexArray(m_vao);
        glDrawArrays(GL_LINES, 0, 2);
    }

    void line_mesh::update(const line& l)
    {
        glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
        glBufferSubData(GL_ARRAY_BUFFER, 0, 2 * sizeof(float), glm::value_ptr(l.start));
        glBufferSubData(GL_ARRAY_BUFFER, 2 * sizeof(float), 2 * sizeof(float), glm::value_ptr(l.end));
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }
}