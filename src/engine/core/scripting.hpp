#ifndef SCRIPTING_HPP
#define SCRIPTING_HPP

#include "ebus.hpp"

#include <chaiscript/chaiscript.hpp>

#include <string_view>

namespace ivn
{
    struct get_script_system_event
    {
        class scripting*& out;
    };

    struct execute_script_event
    {
        const std::string_view script;
    };

    class scripting
    {
    private:
        ebus_collector m_collector{&ebus::get_singleton()};
        chaiscript::ChaiScript m_chai;
    
    public:
        scripting()
        {
            m_collector.listen<get_script_system_event>([this](auto& event)
            {
                event.out = this;
            });

            m_collector.listen<execute_script_event>([this](auto& event)
            {
                m_chai.eval(event.script.data());
            });
        }

        template <typename T>
        void register_function(const T& callable, const std::string_view function_name)
        {
            m_chai.add(chaiscript::fun(callable), function_name.data());
        }
    };
}

#endif
