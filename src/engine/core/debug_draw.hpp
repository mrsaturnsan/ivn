#ifndef DEBUG_DRAW_HPP
#define DEBUG_DRAW_HPP

#include "../components/aabb.hpp"
#include "ebus.hpp"
#include "shader.hpp"
#include "line_mesh.hpp"

#include <memory>

namespace ivn
{
    // EVENTS
    /////////////////////////////////////////
    struct draw_line_event
    {
        const line& l;
        const glm::vec4& color;
    };

    struct draw_aabb_event
    {
        const aabb& box;
        const glm::vec4& color;
    };
    /////////////////////////////////////////

    class debug_drawer
    {
    private:
        ebus_collector m_collector{&ebus::get_singleton()};
        shader m_line_shader;
        line_mesh m_mesh;

    public:
        debug_drawer();

        ~debug_drawer();
        
        void draw_line(const line& l, const glm::vec4& color = 
            glm::vec4{1.0f, 0.0f, 0.0f, 1.0f});

        void draw_bounding_box(const aabb& area, const glm::vec4& color = 
            glm::vec4{1.0f, 0.0f, 0.0f, 1.0f});
    };
}


#endif
