#include "renderer.hpp"
#include "ebus.hpp"
#include "constants.hpp"

#define GLEW_STATIC
#include <GL/glew.h>
#ifdef __APPLE__
    #include <OpenGL/gl.h>
#endif
#include <GLFW/glfw3.h>
#include <glm/gtc/matrix_transform.hpp>

#include <stdexcept>
#include <iostream>

namespace
{
    void APIENTRY GLDebugOutput(GLenum source,
        GLenum type,
        GLuint id,
        GLenum severity,
        [[maybe_unused]] GLsizei length,
        const GLchar* message,
        [[maybe_unused]] const void* userParam) noexcept
    {
        // ignore non-significant error/warning codes
        if (id == 131169 || id == 131185 || id == 131218 || id == 131204) return;

        std::cerr << "---------------" << '\n';
        std::cerr << "Debug message (" << id << "): " << message << '\n';

        switch (source)
        {
            case GL_DEBUG_SOURCE_API:             std::cerr << "Source: API"; break;
            case GL_DEBUG_SOURCE_WINDOW_SYSTEM:   std::cerr << "Source: Window System"; break;
            case GL_DEBUG_SOURCE_SHADER_COMPILER: std::cerr << "Source: Shader Compiler"; break;
            case GL_DEBUG_SOURCE_THIRD_PARTY:     std::cerr << "Source: Third Party"; break;
            case GL_DEBUG_SOURCE_APPLICATION:     std::cerr << "Source: Application"; break;
            case GL_DEBUG_SOURCE_OTHER:           std::cerr << "Source: Other"; break;
        } std::cerr << '\n';

        switch (type)
        {
            case GL_DEBUG_TYPE_ERROR:               std::cerr << "Type: Error"; break;
            case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: std::cerr << "Type: Deprecated Behaviour"; break;
            case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:  std::cerr << "Type: Undefined Behaviour"; break;
            case GL_DEBUG_TYPE_PORTABILITY:         std::cerr << "Type: Portability"; break;
            case GL_DEBUG_TYPE_PERFORMANCE:         std::cerr << "Type: Performance"; break;
            case GL_DEBUG_TYPE_MARKER:              std::cerr << "Type: Marker"; break;
            case GL_DEBUG_TYPE_PUSH_GROUP:          std::cerr << "Type: Push Group"; break;
            case GL_DEBUG_TYPE_POP_GROUP:           std::cerr << "Type: Pop Group"; break;
            case GL_DEBUG_TYPE_OTHER:               std::cerr << "Type: Other"; break;
        } std::cerr << '\n';

        switch (severity)
        {
            case GL_DEBUG_SEVERITY_HIGH:         std::cerr << "Severity: high"; break;
            case GL_DEBUG_SEVERITY_MEDIUM:       std::cerr << "Severity: medium"; break;
            case GL_DEBUG_SEVERITY_LOW:          std::cerr << "Severity: low"; break;
            case GL_DEBUG_SEVERITY_NOTIFICATION: std::cerr << "Severity: notification"; break;
        } std::cerr << '\n';
        std::cerr << '\n';
    }
}

namespace ivn
{
    renderer::renderer(std::int32_t width, std::int32_t height)
    {
        glfwInit();
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
    #ifdef __APPLE__
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    #endif
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_FALSE);
        glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);

        m_window = glfwCreateWindow(width, height, "IVN Editor", nullptr, nullptr);

        if (!m_window)
        {
            glfwTerminate();
            throw std::runtime_error{"Failed to create a window!"};
        }

        glfwGetFramebufferSize(m_window, &width, &height);
        update_viewport(width, height);

        glfwMakeContextCurrent(m_window);
        glfwSetWindowUserPointer(m_window, this);

        glfwSetFramebufferSizeCallback(m_window, [](GLFWwindow* window, GLsizei width, GLsizei height)
        {
            static_cast<renderer*>(glfwGetWindowUserPointer(window))->update_viewport(width, height);
        });

        // Turn on vsync
        glfwSwapInterval(1);

        // Enable additional OpenGL features
        glewExperimental = GL_TRUE;

        glewInit();
        glGetError(); // Call it once to catch glewInit() bug
        
        // Enable GL error callback
        GLint flags;
        glGetIntegerv(GL_CONTEXT_FLAGS, &flags);
        if (flags & GL_CONTEXT_FLAG_DEBUG_BIT)
        {
            glEnable(GL_DEBUG_OUTPUT);
            glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
            glDebugMessageCallback(GLDebugOutput, nullptr);
            glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
        }

        ebus::get_singleton().notify<window_created_event>({m_window});

        m_collector.listen<get_window_size_event>([this](auto& event)
        {
            event.width = m_width;
            event.height = m_height;
        });

        m_collector.listen<get_projection_matrix_event>([this](auto& event)
        {
            event.projection = &m_projection;
        });

        m_collector.listen<get_inverse_position_event>([this](auto& event)
        {
            glm::vec4 in{event.x_pos, event.y_pos, 0.0f, 1.0f};
            glm::vec4 out = m_inv_projection * in;
            event.x_pos = out.x;
            event.y_pos = out.y;
        });

         m_collector.listen<set_clear_color_event>([](auto& event)
         {
            glClearColor(event.color[0], event.color[1], event.color[2], event.color[3]);
         });

        setup_render_state();
    }

    renderer::~renderer() noexcept
    {
        glfwTerminate();
        ebus::get_singleton().notify<window_destroyed_event>({});
    }

    GLFWwindow* renderer::get_window() const noexcept
    {
        return m_window;
    }

    const glm::mat4& renderer::get_projection() const noexcept
    {
        return m_projection;
    }

    void renderer::update_viewport(std::int32_t width, std::int32_t height) noexcept
    {
        m_width = width;
        m_height = height;

        glViewport(0, 0, width, height);

        constexpr float near_plane = -1.0f;
        constexpr float far_plane = 1.0f;
        float ratio =  static_cast<float>(width) / static_cast<float>(height);

        m_projection = glm::ortho(-constants::world_size * ratio, constants::world_size * ratio, 
            -static_cast<float>(constants::world_size), static_cast<float>(constants::world_size), 
            near_plane, far_plane);
         
        m_inv_projection = glm::inverse(m_projection);

        ebus::get_singleton().notify<window_resized_event>({width, height});
    }

    bool renderer::should_close() const noexcept
    {
        return glfwWindowShouldClose(m_window);
    }

    void renderer::begin_frame()
    {
        glfwPollEvents();
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        ebus::get_singleton().notify<begin_frame_event>({});
    }

    void renderer::end_frame()
    {
        ebus::get_singleton().notify<end_frame_event>({});

        glfwSwapBuffers(m_window);
    }

    void renderer::setup_render_state()
    {
        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);
        glFrontFace(GL_CCW);

        glEnable(GL_DEPTH_TEST);

        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        glClearColor(default_clear[0], default_clear[1], 
            default_clear[2], default_clear[3]);
    }
}
