#include "entity.hpp"
#include "scene.hpp"
#include "resource_manager.hpp"
#include "engine.hpp"
#include "mesh.hpp"
#include "shader.hpp"
#include "texture.hpp"

#include <algorithm>

namespace ivn
{
    entity::entity(const std::string& name) : resource{resource_type::ENTITY, name}
    {
        m_collector.listen<resource_removed_event>([this](auto& event)
        {
            remove_resource(event.removed_resource);
        });
    }

    void entity::serialize_out(nlohmann::json& data)
    {
        auto id = std::to_string(get_id());

        auto resources = nlohmann::json::array();

        for (resource* i : m_resources)
        {
            resources.emplace_back(std::to_string(i->get_id()));
        }

        auto info = nlohmann::json::object();
        auto& details = info[id];
        details["Name"] = get_name();
        details["Resources"] = std::move(resources);

        data["Entity"].emplace_back(std::move(info));
    }

    void entity::add_resource(resource* res)
    {
        assert(res->get_type() != resource_type::ENTITY && "Cannot add entity as resource to"
            " another entity!");

        m_resources.emplace_back(res);
        handle_resource_add(res);
    }

    void entity::remove_resource(const resource* res)
    {
        auto it = std::find(m_resources.begin(), m_resources.end(), res);

        if (it != m_resources.end())
        {
            m_resources.erase(it);
            handle_resource_remove(res);
        }
    }

    bool entity::has_resource(const resource* res) const
    {
        return std::find(m_resources.begin(), m_resources.end(), res) != m_resources.end(); 
    }

    bool entity::has_resource(resource_type type) const
    {
        return std::any_of(m_resources.begin(), m_resources.end(), [type](auto& resource)
        {
            return resource->get_type() == type;
        });
    }

    resource_list& entity::get_resources() noexcept
    {
        return m_resources;
    }

    void entity::tick([[maybe_unused]] float dt)
    {
        m_aabb = m_transform;
        m_rc.render(m_transform.get_model());
    }

    void entity::handle_resource_add(resource* res)
    {
        switch (res->get_type())
        {
            case resource_type::TEXTURE:
                m_rc.m_texture = cast<texture*>(res);
                break;

            case resource_type::SHADER:
                m_rc.m_shader = cast<shader*>(res);
                break;

            case resource_type::MESH:
                m_rc.m_mesh = cast<mesh*>(res);
                break;

            case resource_type::AUDIO:
                break;

            case resource_type::SCRIPT:
                break;
            
            default:
                break;
        }
    }

    void entity::handle_resource_remove(const resource* res)
    {
        switch (res->get_type())
        {
            case resource_type::TEXTURE:
                m_rc.m_texture->unbind();
                m_rc.m_texture = nullptr;
                break;

            case resource_type::SHADER:
                m_rc.m_shader = nullptr;
                break;

            case resource_type::MESH:
                m_rc.m_mesh = nullptr;
                break;

            case resource_type::AUDIO:
                break;

            case resource_type::SCRIPT:
                break;
            
            default:
                break;
        }
    }

    std::unique_ptr<entity> entity::serialize_in(const nlohmann::json& data)
    {
        auto new_entity = std::make_unique<entity>(data["Name"]);  

        for (const auto& [key, value] : data["Resources"].items())
        {
            resource* new_res = nullptr;
            ebus::get_singleton().notify<get_resource_from_file_id_event>({value, new_res});

            if (new_res)
            {
                new_entity->add_resource(new_res);
            }
        }

        return new_entity;
    }
}
