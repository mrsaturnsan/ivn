#ifndef EBUS_HPP
#define EBUS_HPP

#include "singleton.hpp"

#include <eventbus/EventBus.h>
#include <eventbus/EventCollector.h>

namespace ivn
{
    class ebus : public singleton<ebus>, public Dexode::EventBus {};

    class ebus_collector : public Dexode::EventCollector {};
}

#endif
