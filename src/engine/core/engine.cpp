#include "engine.hpp"

#include <memory>
#include <chrono>

namespace ivn
{
    engine::engine()
    {
        auto default_scene = std::make_unique<scene>("Default");
        ebus::get_singleton().notify<load_scene_event>({default_scene.get()});
        ebus::get_singleton().notify<resource_add_event>({std::move(default_scene)});
    }

    void engine::execute()
    {
        auto t_start = std::chrono::high_resolution_clock::now();

        while (!m_renderer.should_close())
        {
            m_renderer.begin_frame();
            
            auto t_end = std::chrono::high_resolution_clock::now();
            float delta_time = std::chrono::duration<float, std::ratio<1>>(t_end - t_start).count();
            ebus::get_singleton().notify<tick_event>({delta_time});
            t_start = t_end;

            m_renderer.end_frame();
        }
    }
}
