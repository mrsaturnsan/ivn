#ifndef SCRIPT_HPP
#define SCRIPT_HPP

#include "resource.hpp"

#include <vector>

namespace ivn
{
    class script : public resource
    {
    private:
        std::vector<std::string> m_instructions;
        std::size_t m_instruction_pointer = 0;

    public:
        script(const std::string& name);

        void execute();

        void serialize_out(nlohmann::json& data) override;

        std::vector<std::string>& get_instructions() noexcept;

        std::size_t get_instruction_pointer() const noexcept;

        void set_instruction_pointer(std::size_t instruction_pointer) noexcept;
    
    public:
        static std::unique_ptr<script> serialize_in(const nlohmann::json& data);
    };
}

#endif
