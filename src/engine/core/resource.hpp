#ifndef RESOURCE_HPP
#define RESOURCE_HPP

#include <nlohmann/json/json.hpp>

#include <string>
#include <memory>

namespace ivn
{
    enum class resource_type
    {
        TEXTURE,
        SHADER,
        MESH,
        AUDIO,
        SCRIPT,
        SCENE,
        ENTITY
    };

    class resource
    {
    private:
        resource_type m_type;
        std::string m_name;
        std::size_t m_id = 0;

    public:
        resource(resource_type type, const std::string& name);

        virtual ~resource() = default;

        void set_name(const std::string& name);

        const std::string& get_name() const noexcept;

        resource_type get_type() const noexcept;

        std::size_t get_id() const noexcept;

        virtual void serialize_out(nlohmann::json& data);
    };
}

#endif
