add_library(engine)

target_compile_options(engine
    PRIVATE
        -Wall
        -Wextra
        -Wshadow
        -Wnon-virtual-dtor
        -Wunused
        -Woverloaded-virtual
        -Wpedantic
        -Wformat=2
)

set_target_properties(engine
    PROPERTIES
        CXX_STANDARD 17
        CXX_STANDARD_REQUIRED ON
        CXX_EXTENSIONS OFF
)

target_compile_definitions(engine
    PUBLIC
        DEBUG
)

target_sources(engine
    PRIVATE
        core/engine.cpp
        core/utility.cpp
        core/renderer.cpp
        core/resource.cpp
        core/resource_manager.cpp
        core/sprite_mesh.cpp
        core/shader.cpp
        core/texture.cpp
        components/transform.cpp
        components/aabb.cpp
        components/rendercomp.cpp
        core/entity.cpp
        core/scene.cpp
        core/scene_manager.cpp
        core/input_manager.cpp
        core/debug_draw.cpp
        core/line_mesh.cpp
        core/game_loader.cpp
        core/script.cpp
        components/text.cpp
)

target_include_directories(engine
    PRIVATE
        ${PROJECT_SOURCE_DIR}/libs
        ${GLEW_INCLUDE_DIRS}
    PUBLIC
        ${CMAKE_CURRENT_SOURCE_DIR}
)

target_link_libraries(engine
    PUBLIC
        Dexode::EventBus
        ${OPENGL_gl_LIBRARY}
        ${GLUT_LIBRARIES}
        ${GLEW_LIBRARIES}
        glfw
        glm
)
