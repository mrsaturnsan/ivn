#include "rendercomp.hpp"
#include "../core/shader.hpp"
#include "../core/mesh.hpp"
#include "../core/texture.hpp"
#include "../core/renderer.hpp"

namespace ivn
{
    void rendercomp::render(const glm::mat4& model)
    {
        if (m_shader && m_mesh)
        {
            m_shader->bind();

            glm::mat4* proj = nullptr;
            ebus::get_singleton().notify<get_projection_matrix_event>({proj});

            assert(proj && "Something went wrong with the projection matrix!");

            m_shader->set_mat4("proj", *proj);
            m_shader->set_mat4("model", model);

            if (m_texture)
            {
                m_texture->bind();
            }

            m_mesh->draw();
        }
    }
}
