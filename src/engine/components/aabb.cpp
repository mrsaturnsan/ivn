#include "aabb.hpp"

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <algorithm>
#include <array>

namespace ivn
{
    aabb::aabb(const transform& trs) noexcept
    {
        glm::mat4 model = trs.get_model();

        std::array<glm::vec4, 4> corners
        {
            glm::vec4{-1.0f, -1.0f, 0.0f, 1.0f},
            glm::vec4{-1.0f, 1.0f, 0.0f, 1.0f},
            glm::vec4{1.0f, 1.0f, 0.0f, 1.0f},
            glm::vec4{1.0f, -1.0f, 0.0f, 1.0f}
        };

        for (auto& i : corners)
        {
            i = model * i;
        }

        std::array<float, 2> smallest;
        std::fill(smallest.begin(), smallest.end(), std::numeric_limits<float>::max());

        std::array<float, 2> largest;
        std::fill(largest.begin(), largest.end(), std::numeric_limits<float>::lowest());

        for (std::uint8_t i = 0; i < 4; ++i)
        {
            for (std::uint8_t j = 0; j < 2; ++j)
            {
                smallest[j] = std::min(smallest[j], glm::value_ptr(corners[i])[j]);
                largest[j] = std::max(largest[j], glm::value_ptr(corners[i])[j]);
            }
        }

        glm::vec3 pos = trs.get_position();

        m_x_min = smallest[0];
        m_y_min = smallest[1];
        m_z_min = pos.z - 0.5f;

        m_x_max = largest[0];
        m_y_max = largest[1];
        m_z_max = pos.z + 0.5f;
    }

    aabb& aabb::operator=(const transform& trs) noexcept
    {
        aabb temp{trs};
        *this = temp;
        return *this;
    }

    void aabb::set_min(float x, float y, float z) noexcept
    {
        m_x_min = x;
        m_y_min = y;
        m_z_min = z;
    }

    void aabb::set_max(float x, float y, float z) noexcept
    {
        m_x_max = x;
        m_y_max = y;
        m_z_max = z;
    }

    bool aabb::is_intersecting(const aabb& other) const noexcept
    {
        return (other.m_x_min <= m_x_max && other.m_x_max >= m_x_min) &&
               (other.m_y_min <= m_y_max && other.m_y_max >= m_y_min) &&
               (other.m_z_min <= m_z_max && other.m_z_max >= m_z_min);
    }

    bool aabb::is_intersecting(float x, float y) const noexcept
    {
        return x >= m_x_min 
            && y >= m_y_min 
            && x <= m_x_max  
            && y <= m_y_max;
    }

    bool aabb::is_intersecting(float x, float y, float z) const noexcept
    {
        return x >= m_x_min 
            && y >= m_y_min 
            && z >= m_z_min
            && x <= m_x_max  
            && y <= m_y_max
            && z <= m_z_max;
    }

    std::array<float, 6> aabb::get_bounds() const noexcept
    {
        return {m_x_min, m_y_min, m_z_min, 
                m_x_max, m_y_max, m_z_max};
    }

    std::optional<glm::vec3> aabb::closest_point(glm::vec3 ray_start, glm::vec3 ray_dir)
    {
        glm::vec3 dir_inv = 1.0f / ray_dir;

        double t1 = (m_x_min - ray_start.x) * dir_inv.x;
        double t2 = (m_x_max - ray_start.x) * dir_inv.x;
    
        double t_min = std::min(t1, t2);
        double t_max = std::max(t1, t2);
    
        t1 = (m_y_min - ray_start.y) * dir_inv.y;
        t2 = (m_y_max - ray_start.y) * dir_inv.y;

        t_min = std::max(t_min, std::min(t1, t2));
        t_max = std::min(t_max, std::max(t1, t2));

        t1 = (m_z_min - ray_start.z) * dir_inv.z;
        t2 = (m_z_max - ray_start.z) * dir_inv.z;

        t_min = std::max(t_min, std::min(t1, t2));
        t_max = std::min(t_max, std::max(t1, t2));
    
        if (t_max > std::max(t_min, 0.0))
        {
            return ray_start + ray_dir * static_cast<float>(t_max);
        }
        else
        {
            return std::nullopt;
        }
    }
}
