#ifndef TEXT_HPP
#define TEXT_HPP

#include <string>

namespace ivn
{
    class text
    {
    public:
        float m_wrapping = 30.0f;
        float m_text_size = 15.0f;
        std::string m_text;

    private:

    public:
        void draw_text(const std::string& text);
    };
}

#endif
