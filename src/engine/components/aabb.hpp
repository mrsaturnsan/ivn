#ifndef AABB_HPP
#define AABB_HPP

#include "transform.hpp"

#include <array>
#include <optional>

namespace ivn
{
    class aabb
    {
    private:
        float m_x_min = -0.5f;
        float m_y_min = -0.5f;
        float m_z_min = -0.5f;
        float m_x_max = 0.5f;
        float m_y_max = 0.5f;
        float m_z_max = 0.5f;

    public:
        aabb() = default;

        ~aabb() = default;

        aabb(const aabb&) = default;

        aabb(aabb&&) = default;

        aabb(const transform& trs) noexcept;
        
        aabb& operator=(const aabb&) = default;

        aabb& operator=(const transform& trs) noexcept;

        void set_min(float x, float y, float z) noexcept;

        void set_max(float x, float y, float z) noexcept;

        bool is_intersecting(const aabb& other) const noexcept;

        bool is_intersecting(float x, float y) const noexcept;

        bool is_intersecting(float x, float y, float z) const noexcept;

        std::array<float, 6> get_bounds() const noexcept;

        std::optional<glm::vec3> closest_point(glm::vec3 ray_start, 
                                               glm::vec3 ray_dir);
    };
}

#endif
