#include "transform.hpp"

#include <glm/gtc/matrix_transform.hpp>

namespace ivn
{
    void transform::set_position(const glm::vec3& position) noexcept
    {
        m_position = position;
    }

    void transform::translate(const glm::vec3& offset) noexcept
    {
        m_position += offset;
    }

    glm::vec3 transform::get_position() const noexcept
    {
        return m_position;
    }

    void transform::set_scale(const glm::vec2& scale) noexcept
    {
        m_scale = scale;
    }

    void transform::scale(const glm::vec2& scale_factor) noexcept
    {
        m_scale *= scale_factor;
    }

    glm::vec2 transform::get_scale() const noexcept
    {
        return m_scale;
    }

    void transform::set_rotation(float degrees) noexcept
    {
        m_rotation = degrees;
    }

    void transform::rotate(float degrees) noexcept
    {
        m_rotation += degrees;
    }

    float transform::get_rotation() const noexcept
    {
        return m_rotation;
    }

    glm::mat4 transform::get_model() const noexcept
    {
        glm::mat4 model{1.0f};

        model = glm::translate(model, m_position);
        model = glm::rotate(model, glm::radians(m_rotation), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3{m_scale, 1.0f});
        
        return model;
    }
}
