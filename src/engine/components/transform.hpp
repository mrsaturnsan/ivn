#ifndef TRANSFORM_HPP
#define TRANSFORM_HPP

#include <glm/glm.hpp>

namespace ivn
{
    class transform
    {
    private:
        glm::vec3 m_position{0.0f, 0.0f, 0.0f};
        glm::vec2 m_scale{1.0f, 1.0f};
        float m_rotation = 0.0f;

    public:
        void set_position(const glm::vec3& position) noexcept;

        void translate(const glm::vec3& offset) noexcept;

        glm::vec3 get_position() const noexcept;

        void set_scale(const glm::vec2& scale) noexcept;

        void scale(const glm::vec2& scale_factor) noexcept;

        glm::vec2 get_scale() const noexcept;

        void set_rotation(float degrees) noexcept;

        void rotate(float degrees) noexcept;

        float get_rotation() const noexcept;

        glm::mat4 get_model() const noexcept;
    };
}

#endif
