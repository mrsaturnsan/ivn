#ifndef RENDERCOMP_HPP
#define RENDERCOMP_HPP

#include <glm/glm.hpp>

namespace ivn
{
    class shader;
    class mesh;
    class texture;

    struct rendercomp
    {
    public:
        shader* m_shader = nullptr;
        mesh* m_mesh = nullptr;
        texture* m_texture = nullptr;

        void render(const glm::mat4& model);
    };
}

#endif
