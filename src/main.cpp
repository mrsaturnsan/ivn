#include <core/engine.hpp>
#include "editor/editor.hpp"

#include <stdexcept>
#include <iostream>

int main()
{
    try
    {
        ivn::editor::editor editor;
        ivn::engine engine;
        engine.execute();
    }
    catch (const std::exception& e)
    {
        std::cerr << e.what() << '\n';
        return -1;
    }
    catch (...)
    {
        std::cerr << "An unknown error has occured." << '\n';
        return -1;
    }
    
    return 0;
}
