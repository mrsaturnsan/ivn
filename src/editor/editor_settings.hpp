#ifndef EDITOR_SETTINGS_HPP
#define EDITOR_SETTINGS_HPP

#include "editor.hpp"

#include <array>
#include <string>

namespace ivn::editor
{
    struct settings_closed_event {};

    class editor_settings : public editor_component
    {
    private:
        std::string m_game_name;
        std::string m_creator_name;
        std::array<float, 4> m_clear_color;
        bool m_show_demo_window = false;

    public:
        ~editor_settings() override;

    protected:
        void draw() override;
    };
}

#endif
