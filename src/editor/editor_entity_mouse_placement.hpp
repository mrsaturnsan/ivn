#ifndef EDITOR_ENTITY_MOUSE_PLACEMENT_HPP
#define EDITOR_ENTITY_MOUSE_PLACEMENT_HPP

#include <core/ebus.hpp>

#include <glm/glm.hpp>

namespace ivn
{
    class entity;
}

namespace ivn::editor
{
    class entity_mouse_placement
    {
    private:
        ebus_collector m_collector{&ebus::get_singleton()};
        entity* m_active_entity = nullptr;
        glm::vec2 m_e_offset;

    public:
        entity_mouse_placement();

        void update_entity_pos();

    private:
        void get_mouse_entity();
    };
}

#endif
