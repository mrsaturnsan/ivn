#ifndef EDITOR_SCENE_EDITOR_HPP
#define EDITOR_SCENE_EDITOR_HPP

#include "editor.hpp"

namespace ivn
{
    class entity;
    class scene;
}

namespace ivn::editor
{
    struct scene_editor_closed_event {};

    struct set_scene_filter_event
    {
        const std::string& filter;
    };

    class scene_editor : public editor_component
    {
    private:
        std::string m_find_res_name;
        std::string m_find_entity_name;
        std::string m_new_scene_name;

    public:
        ~scene_editor() override;

    protected:
        void draw() override;

    private:
        void draw_create_scene();

        void draw_scene_tree();

        void draw_scene_attached_entities(std::vector<entity*>& e, std::int32_t& id);

        void draw_scene_entity_attacher(scene& s, std::int32_t& id);
    };
}

#endif
