#ifndef EDITOR_RESOURCE_MANAGER_HPP
#define EDITOR_RESOURCE_MANAGER_HPP

#include "editor.hpp"

#include <string>
#include <unordered_map>

namespace ivn
{
    class resource;
}

namespace ivn::editor
{
    struct erm_closed_event {};

    struct close_delete_warning_event {};

    struct close_rename_window_event {};

    class editor_delete_resource_warning : public editor_component
    {
    private:
        resource* m_resource = nullptr;

    public:
        editor_delete_resource_warning(resource* res);

    protected:
        void draw() override;
    };

    class editor_rename_resource : public editor_component
    {
    private:
        resource* m_resource = nullptr;
        std::string m_new_name;

    public:
        editor_rename_resource(resource* res);

    protected:
        void draw() override;
    };

    class editor_resource_manager : public editor_component
    {
    private:
        std::string m_resource_path;
        std::string m_resource_name;
        std::string m_find_res_name;

    public:
        editor_resource_manager();
        
        ~editor_resource_manager() override;

    protected:
        void draw() override;

    private:
        void draw_load_resource();

        void draw_resource_list();

        void create_resource();
    };
}

#endif
