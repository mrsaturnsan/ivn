#include "editor_serializer.hpp"
#include <core/resource_manager.hpp>
#include "editor_error_popup.hpp"
#include <core/game_loader.hpp>

#include <imgui.h>
#include <imgui_stdlib.h>

#ifdef _WIN32
    #include <direct.h>
    #define GETCURRENTDIRECTORY _getcwd
#else
    #include <unistd.h>
    #define GETCURRENTDIRECTORY getcwd
#endif

#include <array>
#include <fstream>
#include <iomanip>

namespace ivn::editor
{
    serializer::serializer()
    {
        std::array<char, FILENAME_MAX> buffer;
        GETCURRENTDIRECTORY(buffer.data(), FILENAME_MAX);
        m_file_path = buffer.data();
    }

    serializer::~serializer()
    {
        ebus::get_singleton().notify<serializer_closed_event>({});
    }

    void serializer::draw()
    {
        if (ImGui::Begin("Serializer", &m_active))
        {
            ImGui::TextWrapped("You can save and load your data from here.");

            ImGui::Separator();

            ImGui::InputText("File Path", &m_file_path);

            if (ImGui::Button("Save") && !m_file_path.empty())
            {
                save_game();
            }

            ImGui::SameLine();

            if (ImGui::Button("Load") && !m_file_path.empty())
            {
                load_game();
            }
        }

        ImGui::End();

        draw_children();
    }

    void serializer::load_game()
    {
        ebus::get_singleton().notify<load_game_from_file_event>({m_file_path});
    }

    void serializer::save_game()
    {
        nlohmann::json data;

        resource_table* table;
        ebus::get_singleton().notify<resource_manager_get_all_resources>({table});
        
        for (const auto& [name, res] : *table)
        {
            res->serialize_out(data);
        }

        if (data.empty())
        {
            return;
        }

        std::ofstream out{m_file_path};

        if (!out)
        {
            add_component(std::make_unique<editor_error_popup>("Unable to open: " + m_file_path));
            return;
        }
        
        out << std::setw(4) << data << std::endl;
    }
}
