#ifndef EDITOR_ROOT_HPP
#define EDITOR_ROOT_HPP

#include "editor.hpp"

namespace ivn::editor
{
    struct open_scene_editor_event {};

    struct open_entity_editor_event {};
    
    class editor_root : public editor_component
    {
    private:
        bool m_resource_manager_open = false;
        bool m_profiler_open = false;
        bool m_settings_open = false;
        bool m_scene_editor_open = false;
        bool m_entity_editor_open = false;
        bool m_serializer_open = false;
        bool m_chai_editor_open = false;
        
    public:
        editor_root();
        
    protected:
        void draw() override;

    private:
        void handle_resource_manager();

        void handle_profiler();

        void handle_settings();

        void handle_scene_editor();

        void handle_entity_editor();

        void handle_serializer();

        void handle_chai_editor();
    };
}

#endif
