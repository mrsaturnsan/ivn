#ifndef EDITOR_ENTITY_EDITOR_HPP
#define EDITOR_ENTITY_EDITOR_HPP

#include "editor.hpp"
#include "editor_entity_mouse_placement.hpp"

namespace ivn
{
    class entity;
}

namespace ivn::editor
{
    struct close_entity_editor_event {};

    struct set_entity_filter_event
    {
        const std::string& filter;
    };

    class entity_editor : public editor_component
    {
    private:
        std::string m_new_entity_name;
        std::string m_find_res_name;
        std::string m_find_enity_res_name;
        entity_mouse_placement m_mouse_placer;

    public:
        entity_editor();

        ~entity_editor() override;

    protected:
        void draw() override;

        void draw_create_entity();

        void draw_entity_tree();

        void draw_component_attacher(std::int32_t& id, entity* e);

        void draw_transform_editor(entity* e);

        void draw_text_editor(entity* e);

        void move_entity_with_mouse();
    };
}

#endif
