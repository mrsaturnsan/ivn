#include "editor_entity_editor.hpp"
#include "editor_resource_manager.hpp"
#include <core/entity.hpp>
#include <core/resource_manager.hpp>
#include <core/texture.hpp>
#include <core/scene_manager.hpp>

#include <glm/gtc/type_ptr.hpp>

#include <imgui.h>
#include <imgui_stdlib.h>

namespace ivn::editor
{
    entity_editor::entity_editor()
    {
        m_collector.listen<set_entity_filter_event>([this](auto& event)
        {
            m_find_res_name = event.filter;
        });
    }

    entity_editor::~entity_editor()
    {
        ebus::get_singleton().notify<close_entity_editor_event>({});
    }

    void entity_editor::draw()
    {
        if (ImGui::Begin("Entity Editor", &m_active))
        {
            m_mouse_placer.update_entity_pos();
            
            ImGui::TextWrapped("Build and edit entities here! They can be attached to scenes to create levels."
            " Also, you can attach components to entities to get different behaviors.");

            ImGui::Separator();

            draw_create_entity();

            ImGui::Separator();

            draw_entity_tree();
        }

        ImGui::End();

        draw_children();
    }

    void entity_editor::draw_create_entity()
    {
        if (ImGui::CollapsingHeader("Create New Entity", ImGuiTreeNodeFlags_DefaultOpen))
        {
            ImGui::InputText("Entity Name", &m_new_entity_name);

            if (ImGui::Button("Create Entity") && !m_new_entity_name.empty())
            {
                ebus::get_singleton().notify<resource_add_event>({std::make_unique<entity>(m_new_entity_name)});
            }
        }
    }
    
    void entity_editor::draw_entity_tree()
    {
        if (ImGui::CollapsingHeader("Entity List", ImGuiTreeNodeFlags_DefaultOpen))
        {
            ImGui::InputText("Filter", &m_find_res_name);

            resource_table* table;
            ebus::get_singleton().notify<resource_manager_get_all_resources>({table});

            std::int32_t id = 0;
            
            for (const auto& [name, res] : *table)
            {
                if (res->get_type() != resource_type::ENTITY || 
                    name.find(m_find_res_name) == std::string::npos)
                {
                    continue;
                }

                ImGui::PushID(ImGui::GetID(this + id++));

                if (ImGui::TreeNodeEx(name.c_str(), ImGuiTreeNodeFlags_DefaultOpen))
                {
                    entity* e = cast<entity*>(res.get());

                    ImGui::SameLine();

                    if (ImGui::SmallButton("Delete"))
                    {
                        add_component(std::make_unique<editor_delete_resource_warning>(e));
                    }

                    ImGui::SameLine();

                    if (ImGui::SmallButton("Rename"))
                    {
                        add_component(std::make_unique<editor_rename_resource>(e));
                    }

                    scene* s = nullptr;
                    ebus::get_singleton().notify<get_active_scene_event>({s});

                    if (s)
                    {
                        ImGui::SameLine();

                        if (!s->contains_entity(e))
                        {
                            if (ImGui::SmallButton("Add to active scene"))
                            {
                                s->add_entity(e);
                            }
                        }
                        else
                        {
                            if (ImGui::SmallButton("Remove from active scene"))
                            {
                                s->remove_entity(e);
                            }
                        }
                    }

                    auto& entity_resource = e->get_resources();

                    for (auto& e_rsc : entity_resource)
                    {
                        if (ImGui::TreeNode(e_rsc->get_name().c_str()))
                        {
                            if (e_rsc->get_type() == resource_type::TEXTURE)
                            {
                                texture* tex = cast<texture*>(e_rsc);
                                ImTextureID tex_id = reinterpret_cast<void*>(tex->get_id());
                                ImGui::Image(tex_id, ImVec2{128, 128}, ImVec2{0, 1}, ImVec2{1, 0});
                            }

                            if (ImGui::Button("Detach"))
                            {
                                e->remove_resource(e_rsc);
                                ImGui::TreePop();
                                break;
                            }

                            ImGui::TreePop();
                        }
                    }

                    ImGui::Separator();

                    draw_transform_editor(e);

                    draw_text_editor(e);

                    draw_component_attacher(id, e);

                    ImGui::TreePop();
                }

                ImGui::PopID();
            }
        }
    }

    void entity_editor::draw_component_attacher(std::int32_t& id, entity* e)
    {
        ImGui::InputText("Filter", &m_find_enity_res_name);

        resource_table* table;
        ebus::get_singleton().notify<resource_manager_get_all_resources>({table});
        
        for (const auto& [name, res] : *table)
        {
            resource_type type = res->get_type();

            // don't let entities and scenes be attached to entities...
            if (type == resource_type::ENTITY || type == resource_type::SCENE || 
                type == resource_type::SCRIPT || name.find(m_find_enity_res_name) == std::string::npos) continue;

            if (e->has_resource(type))
            {
                continue;
            }

            ImGui::PushID(ImGui::GetID(this + id++));

            if (ImGui::TreeNode(name.c_str()))
            {
                if (res->get_type() == resource_type::TEXTURE)
                {
                    texture* tex = cast<texture*>(res.get());
                    ImTextureID tex_id = reinterpret_cast<void*>(tex->get_id());
                    ImGui::Image(tex_id, ImVec2{128, 128}, ImVec2{0, 1}, ImVec2{1, 0});
                }

                if (ImGui::Button("Attach"))
                {
                    e->add_resource(res.get());
                }

                ImGui::TreePop();
            }

            ImGui::PopID();
        }
    }

    void entity_editor::draw_transform_editor(entity* e)
    {
        glm::vec3 pos = e->m_transform.get_position();
        
        ImGui::InputFloat2("Position", glm::value_ptr(pos));
        ImGui::SliderFloat("Draw Priority", &pos.z, -0.99f, 0.99f);

        e->m_transform.set_position(pos);

        glm::vec2 scale = e->m_transform.get_scale();
        ImGui::InputFloat2("Scale", glm::value_ptr(scale));
        e->m_transform.set_scale(scale);

        float rotation = e->m_transform.get_rotation();
        ImGui::SliderFloat("Rotation", &rotation, 0.0f, 359.9f);
        e->m_transform.set_rotation(rotation);
    }

    void entity_editor::draw_text_editor(entity* e)
    {
        ImGui::InputTextMultiline("Text", &e->m_text.m_text);
    }
}
