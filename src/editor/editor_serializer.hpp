#ifndef EDITOR_SERIALIZER_HPP
#define EDITOR_SERIALIZER_HPP

#include "editor.hpp"

#include <string>

namespace ivn::editor
{
    struct serializer_closed_event {};

    class serializer : public editor_component
    {
    private:
        std::string m_file_path;

    public:
        serializer();
        
        ~serializer() override;

    protected:
        void draw() override;

    private:
        void load_game();

        void save_game();
    };
}

#endif
