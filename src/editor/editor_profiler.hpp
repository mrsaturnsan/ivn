#ifndef EDITOR_PROFILER_HPP
#define EDITOR_PROFILER_HPP

#include "editor.hpp"

namespace ivn::editor
{
    struct profiler_closed_event {};

    class editor_profiler : public editor_component
    {
    public:
        ~editor_profiler() override;

    protected:
        void draw() override;
    };
}

#endif
