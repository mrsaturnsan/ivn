#include "editor_entity_mouse_placement.hpp"
#include <core/input_manager.hpp>
#include <core/resource_manager.hpp>
#include <core/entity.hpp>
#include <core/debug_draw.hpp>
#include "editor_entity_editor.hpp"
#include <core/scene_manager.hpp>

#include <imgui.h>

#include <limits>

namespace ivn::editor
{
    entity_mouse_placement::entity_mouse_placement()
    {
        m_collector.listen<mouse_down_event>([this](auto&)
        {
            get_mouse_entity();
        });

        m_collector.listen<mouse_up_event>([this](auto&)
        {
            m_active_entity = nullptr;
        });

        m_collector.listen<resource_removed_event>([this](auto& event)
        {
            if (event.removed_resource == m_active_entity)
            {
                m_active_entity = nullptr;
            }
        });
    }

    void entity_mouse_placement::update_entity_pos()
    {
        if (m_active_entity)
        {
            float mouse_x = 0.0f;
            float mouse_y = 0.0f;
            ebus::get_singleton().notify<get_mouse_position_event>({mouse_x, mouse_y});

            float z = m_active_entity->m_transform.get_position().z;

            m_active_entity->m_transform.set_position(glm::vec3{mouse_x, mouse_y, z} - glm::vec3{m_e_offset, 0.0f});
            m_active_entity->m_aabb = m_active_entity->m_transform;

            ebus::get_singleton().notify<draw_aabb_event>({m_active_entity->m_aabb, glm::vec4{1.0f, 0.0f, 0.0f, 1.0f}});
        }
    }

    void entity_mouse_placement::get_mouse_entity()
    {
        // make sure mouse isn't over io!
        ImGuiIO& io = ImGui::GetIO();

        if (io.WantCaptureMouse)
        {
            return;
        }

        scene* active_scene;
        ebus::get_singleton().notify<get_active_scene_event>({active_scene});

        if (!active_scene)
        {
            return;
        }

        std::vector<entity*> scene_entities = active_scene->get_entity_list();

        float furthest = std::numeric_limits<float>::lowest();

        float mouse_x = 0.0f;
        float mouse_y = 0.0f;
        ebus::get_singleton().notify<get_mouse_position_event>({mouse_x, mouse_y});

        for (auto i : scene_entities)
        {       
            if (i->m_aabb.is_intersecting(mouse_x, mouse_y))
            {
                glm::vec3 e_pos = i->m_transform.get_position();

                if (e_pos.z > furthest)
                {
                    furthest = e_pos.z;
                    m_active_entity = i;
                    m_e_offset = glm::vec2{mouse_x, mouse_y} - glm::vec2{e_pos.x, e_pos.y};
                }
            }
        }

        if (m_active_entity)
        {
            ebus::get_singleton().notify<set_entity_filter_event>({m_active_entity->get_name()});
        }
    }
}