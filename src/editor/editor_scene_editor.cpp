#include "editor_scene_editor.hpp"
#include "editor_resource_manager.hpp"
#include <core/scene.hpp>
#include <core/entity.hpp>
#include <core/resource_manager.hpp>
#include <core/scene_manager.hpp>

#include <imgui.h>
#include <imgui_stdlib.h>

namespace ivn::editor
{
    scene_editor::~scene_editor()
    {
        ebus::get_singleton().notify<scene_editor_closed_event>({});

        m_collector.listen<set_scene_filter_event>([this](auto& event)
        {
            m_find_res_name = event.filter;
        });
    }

    void scene_editor::draw()
    {
        if (ImGui::Begin("Scene Editor", &m_active))
        {
            ImGui::TextWrapped("Build and edit your scenes here! You can create, delete, rename, attach, and detach entities from here.");
            
            ImGui::Separator();

            draw_create_scene();
            
            if (ImGui::CollapsingHeader("Scene Hierarchies", ImGuiTreeNodeFlags_DefaultOpen))
            {
                ImGui::Separator();
                draw_scene_tree();
            }
        }

        ImGui::End();

        draw_children();
    }

    void scene_editor::draw_create_scene()
    {
        if (ImGui::CollapsingHeader("Create New Scene", ImGuiTreeNodeFlags_DefaultOpen))
        {
            ImGui::InputText("Scene Name", &m_new_scene_name);

            if (ImGui::Button("Create Scene") && !m_new_scene_name.empty())
            {
                ebus::get_singleton().notify<resource_add_event>({std::make_unique<scene>(m_new_scene_name)});
            }
        }
    }

    void scene_editor::draw_scene_tree()
    {
        ImGui::InputText("Filter", &m_find_res_name);

        resource_table* table;
        ebus::get_singleton().notify<resource_manager_get_all_resources>({table});

        std::int32_t i = 0;

        for (const auto& [name, res] : *table)
        {
            if (res->get_type() != resource_type::SCENE || 
                name.find(m_find_res_name) == std::string::npos)
            {
                continue;
            }

            ImGui::PushID(ImGui::GetID(this + i++));

            if (ImGui::TreeNode(name.c_str()))
            {
                scene* s = cast<scene*>(res.get());

                if (ImGui::SmallButton("Delete Scene"))
                {
                    add_component(std::make_unique<editor_delete_resource_warning>(s));
                }

                ImGui::SameLine();

                if (ImGui::SmallButton("Rename Scene"))
                {
                    add_component(std::make_unique<editor_rename_resource>(s));
                }

                ImGui::SameLine();

                if (ImGui::SmallButton("Load Scene"))
                {
                    ebus::get_singleton().notify<load_scene_event>({s});
                }

                ImGui::Separator();

                script* scr = s->get_active_script();

                if (scr)
                {
                    ImGui::TextWrapped("Active Script: %s", scr->get_name().c_str());

                    ImGui::Separator();
                }

                draw_scene_attached_entities(s->get_entity_list(), i);

                draw_scene_entity_attacher(*s, i);

                ImGui::TreePop();
            }

            ImGui::PopID();
        }
    }

    void scene_editor::draw_scene_attached_entities(std::vector<entity*>& e, std::int32_t& id)
    {
        for (auto it = e.begin(); it < e.end();)
        {
            ImGui::PushID(ImGui::GetID(this + id++));

            if (ImGui::TreeNodeEx((*it)->get_name().c_str(), ImGuiTreeNodeFlags_DefaultOpen))
            {
                if (ImGui::Button("Delete Entity"))
                {
                    it = e.erase(it);
                    ImGui::TreePop();
                    ImGui::PopID();
                    continue;
                }

                ImGui::TreePop();
            }

            ImGui::PopID();

            ++it;
        }
    }

    void scene_editor::draw_scene_entity_attacher(scene& s, std::int32_t& id)
    {
        ImGui::TextWrapped("Attach entities!");
        ImGui::Separator();

        ImGui::InputText("Filter", &m_find_entity_name);

        resource_table* table;
        ebus::get_singleton().notify<resource_manager_get_all_resources>({table});

        for (const auto& [name, res] : *table)
        {
            if (res->get_type() != resource_type::ENTITY || 
                name.find(m_find_entity_name) == std::string::npos ||
                s.has_entity(cast<entity*>(res.get())))
            {
                continue;
            }

            ImGui::PushID(ImGui::GetID(this + id++));

            if (ImGui::TreeNode(name.c_str()))
            {
                if (ImGui::Button("Add Entity"))
                {
                    s.add_entity(cast<entity*>(res.get()));
                }

                ImGui::TreePop();
            }

            ImGui::PopID();
        }
    }
}
