#include "editor.hpp"
#include <core/renderer.hpp>
#include "editor_root.hpp"
#include <core/engine.hpp>

#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>

namespace ivn::editor
{
    void editor_component::draw_children()
    {
        for (auto it = m_components.begin(); it < m_components.end();)
        {
            if (!(*it)->m_active)
            {
                it = m_components.erase(it);
            }
            else
            {
                (*it)->draw();
                ++it;
            }
        }
    }
    
    void editor_component::add_component(std::unique_ptr<editor_component> new_component)
    {
        m_components.emplace_back(std::move(new_component));
    }

    editor::editor() : m_root{std::make_unique<editor_root>()}
    {
        m_collector.listen<window_created_event>([this](auto& event)
        {
            init_editor(event.window);
        });

        m_collector.listen<window_destroyed_event>([this](auto&)
        {
            destroy_editor();
        });
        
        m_collector.listen<begin_frame_event>([this](auto&)
        {
            begin_frame();
        });

        m_collector.listen<tick_event>([this](auto&)
        {
            draw_editor();
        });

        m_collector.listen<end_frame_event>([this](auto&)
        {
            end_frame();
        });
    }

    void editor::init_editor(GLFWwindow* window)
    {
        IMGUI_CHECKVERSION();
        ImGui::CreateContext();
        ImGui::StyleColorsDark();
        ImGui_ImplGlfw_InitForOpenGL(window, true);

        constexpr const char* glsl_version = "#version 410";
        ImGui_ImplOpenGL3_Init(glsl_version);
    }

    void editor::destroy_editor()
    {
        ImGui_ImplOpenGL3_Shutdown();
        ImGui_ImplGlfw_Shutdown();
        ImGui::DestroyContext();
    }

    void editor::begin_frame()
    {
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();
    }

    void editor::draw_editor()
    {
        m_root->draw();
    }

    void editor::end_frame()
    {
        ImGui::Render();
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
    }
}
