#include "editor_error_popup.hpp"

#include <imgui.h>

namespace ivn::editor
{
    editor_error_popup::editor_error_popup(const std::string& error_message) : m_error_message{error_message}
    {
        m_collector.listen<close_error_popup_window_event>([this](auto&)
        {
            m_active = false;
        });
    }

    void editor_error_popup::draw()
    {
        if (ImGui::Begin("Error", &m_active))
        {
            ImGui::TextWrapped("%s", m_error_message.c_str());
        }

        ImGui::End();

        if (!m_active)
        {
            ebus::get_singleton().notify<close_error_popup_window_event>({});
        }
    }
}
