#include "editor_chai_editor.hpp"
#include <core/script.hpp>
#include <core/resource_manager.hpp>
#include "editor_resource_manager.hpp"
#include <core/scene.hpp>

#include <imgui.h>
#include <imgui_stdlib.h>

namespace ivn::editor
{
    chai_editor::~chai_editor()
    {
        ebus::get_singleton().notify<chai_editor_closed_event>({});
    }
    
    void chai_editor::draw()
    {
        if (ImGui::Begin("Chai Editor", &m_active))
        {
            ImGui::TextWrapped("Create scripts and dialogs here! They can be attached to scenes, "
            "and you can add references to different entities as well.");

            ImGui::Separator();

            draw_bound_entity_table();

            ImGui::Separator();

            draw_create_script();

            ImGui::Separator();

            draw_script_tree();
        }

        ImGui::End();
    }

    void chai_editor::draw_bound_entity_table()
    {

    }

    void chai_editor::draw_create_script()
    {
        if (ImGui::CollapsingHeader("Create New Script", ImGuiTreeNodeFlags_DefaultOpen))
        {
            ImGui::InputText("Script Name", &m_script_name);

            if (ImGui::Button("Create Script") && !m_script_name.empty())
            {
                ebus::get_singleton().notify<resource_add_event>({std::make_unique<script>(m_script_name)});
            }
        }
    }

    void chai_editor::draw_script_tree()
    {
        if (ImGui::CollapsingHeader("Script List", ImGuiTreeNodeFlags_DefaultOpen))
        {
            ImGui::InputText("Filter", &m_find_script_name);

            resource_table* table;
            ebus::get_singleton().notify<resource_manager_get_all_resources>({table});

            std::int32_t id = 0;
            
            for (const auto& [name, res] : *table)
            {
                if (res->get_type() != resource_type::SCRIPT || 
                    name.find(m_find_script_name) == std::string::npos)
                {
                    continue;
                }

                ImGui::PushID(ImGui::GetID(this + id++));

                if (ImGui::TreeNodeEx(name.c_str(), ImGuiTreeNodeFlags_DefaultOpen))
                {
                    script* scr = cast<script*>(res.get());

                    ImGui::SameLine();

                    if (ImGui::SmallButton("Delete"))
                    {
                        add_component(std::make_unique<editor_delete_resource_warning>(scr));
                    }

                    ImGui::SameLine();

                    if (ImGui::SmallButton("Rename"))
                    {
                        add_component(std::make_unique<editor_rename_resource>(scr));
                    }

                    ImGui::SameLine();

                    if (ImGui::SmallButton("Set as root script"))
                    {
                        ebus::get_singleton().notify<scene_set_root_script_event>({scr});
                    }

                    ImGui::SameLine();

                    auto& instructions = scr->get_instructions();

                    if (ImGui::SmallButton("Add Instructions"))
                    {
                        instructions.emplace_back();
                    }

                    for (auto it = instructions.begin(); it < instructions.end();)
                    {
                        ImGui::PushID(ImGui::GetID(this + id++));

                        ImGui::InputTextMultiline("Script", &(*it));

                        if (it != instructions.begin())
                        {
                            if (ImGui::SmallButton("Move Up"))
                            {
                                std::iter_swap(it, std::next(it, -1));
                            }
                        }

                        if (it != (instructions.end() - 1))
                        {
                            if (ImGui::SmallButton("Move Down"))
                            {
                                std::iter_swap(it, std::next(it, 1));
                            }
                        }

                        if (ImGui::SmallButton("Delete"))
                        {
                            it = instructions.erase(it);
                        }
                        else
                        {
                            ++it;
                        }

                        ImGui::PopID();

                        ImGui::Separator();
                    }

                    ImGui::TreePop();
                }

                ImGui::PopID();
            }
        }
    }
}
