#include "editor_profiler.hpp"

#include <imgui.h>

namespace ivn::editor
{
    editor_profiler::~editor_profiler()
    {
        ebus::get_singleton().notify<profiler_closed_event>({});
    }

    void editor_profiler::draw()
    {
        if (ImGui::Begin("Profiler", &m_active))
        {
            ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 
                1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
        }

        ImGui::End();
    }
}
