#include "editor_root.hpp"
#include <core/renderer.hpp>
#include "editor_resource_manager.hpp"
#include "editor_error_popup.hpp"
#include "editor_profiler.hpp"
#include "editor_settings.hpp"
#include "editor_scene_editor.hpp"
#include "editor_entity_editor.hpp"
#include "editor_serializer.hpp"
#include "editor_chai_editor.hpp"

#include <imgui.h>

namespace ivn::editor
{
    editor_root::editor_root()
    {
        m_collector.listen<erm_closed_event>([this](auto&)
        {
            m_resource_manager_open = false;
        });

        m_collector.listen<profiler_closed_event>([this](auto&)
        {
            m_profiler_open = false;
        });

        m_collector.listen<settings_closed_event>([this](auto&)
        {
            m_settings_open = false;
        });

        m_collector.listen<scene_editor_closed_event>([this](auto&)
        {
            m_scene_editor_open = false;
        });

        m_collector.listen<close_entity_editor_event>([this](auto&)
        {
            m_entity_editor_open = false;
        });

        m_collector.listen<serializer_closed_event>([this](auto&)
        {
            m_serializer_open = false;
        });

        m_collector.listen<chai_editor_closed_event>([this](auto&)
        {
            m_chai_editor_open = false;
        });
        
        m_collector.listen<open_scene_editor_event>([this](auto&)
        {
            handle_scene_editor();
        });

        m_collector.listen<open_entity_editor_event>([this](auto&)
        {
            handle_entity_editor();
        });
    }
        
    void editor_root::draw()
    {     
        if (ImGui::BeginMainMenuBar())
        {
            if (ImGui::BeginMenu("File"))
            {
                if (ImGui::MenuItem("Settings"))
                {
                    handle_settings();
                }

                if (ImGui::MenuItem("Resource Manager"))
                {
                    handle_resource_manager();
                }

                if (ImGui::MenuItem("Serializer"))
                {
                    handle_serializer();
                }

                ImGui::EndMenu();
            }

            if (ImGui::BeginMenu("Edit Tools"))
            {
                if (ImGui::MenuItem("Scene Editor"))
                {
                    handle_scene_editor();
                }

                if (ImGui::MenuItem("Entity Editor"))
                {
                    handle_entity_editor();
                }

                if (ImGui::MenuItem("Chai Editor"))
                {
                    handle_chai_editor();
                }

                ImGui::EndMenu();
            }

            if (ImGui::BeginMenu("Debug Tools"))
            {
                if (ImGui::MenuItem("Profiler"))
                {
                    handle_profiler();
                }

                ImGui::EndMenu();
            }

            ImGui::EndMainMenuBar();
        }

        draw_children();
    }

    void editor_root::handle_resource_manager()
    {
        if (m_resource_manager_open)
        {
            ImGui::SetWindowFocus("Resource Manager");
        }
        else
        {
            m_resource_manager_open = true;
            add_component(std::make_unique<editor_resource_manager>());
        }
    }

    void editor_root::handle_profiler()
    {
        if (m_profiler_open)
        {
            ImGui::SetWindowFocus("Profiler");
        }
        else
        {
            m_profiler_open = true;
            add_component(std::make_unique<editor_profiler>());
        }
    }

    void editor_root::handle_settings()
    {
        if (m_settings_open)
        {
            ImGui::SetWindowFocus("Settings");
        }
        else
        {
            add_component(std::make_unique<editor_settings>());
            m_settings_open = true;
        }
    }

    void editor_root::handle_scene_editor()
    {
        if (m_scene_editor_open)
        {
            ImGui::SetWindowFocus("Scene Editor");
        }
        else
        {
            m_scene_editor_open = true;
            add_component(std::make_unique<scene_editor>());
        }
    }

    void editor_root::handle_entity_editor()
    {
        if (m_entity_editor_open)
        {
            ImGui::SetWindowFocus("Entity Editor");
        }
        else
        {
            m_entity_editor_open = true;
            add_component(std::make_unique<entity_editor>());
        }
    }

    void editor_root::handle_serializer()
    {
        if (m_serializer_open)
        {
            ImGui::SetWindowFocus("Serializer");
        }
        else
        {
            m_serializer_open = true;
            add_component(std::make_unique<serializer>());
        }
    }

    void editor_root::handle_chai_editor()
    {
        if (m_chai_editor_open)
        {
            ImGui::SetWindowFocus("Chai Editor");
        }
        else
        {
            m_chai_editor_open = true;
            add_component(std::make_unique<chai_editor>());
        }
    }
}
