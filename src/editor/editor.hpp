#ifndef EDITOR_HPP
#define EDITOR_HPP

#include <core/utility.hpp>
#include <core/ebus.hpp>

#include <memory>

struct GLFWwindow;

namespace ivn::editor
{
    class editor_component
    {
    protected:
        ebus_collector m_collector{&ebus::get_singleton()};
        bool m_active = true;

    private:
        std::vector<std::unique_ptr<editor_component>> m_components;

    public:
        virtual ~editor_component() = default;

        virtual void draw() = 0;

    protected:
        void draw_children();

        void add_component(std::unique_ptr<editor_component> new_component);
    };

    class editor
    {
        NO_CPY_MV_ASGN(editor)

    private:
        ebus_collector m_collector{&ebus::get_singleton()};
        std::unique_ptr<editor_component> m_root;

    public:
        editor();

    private:
        void init_editor(GLFWwindow* window);

        void destroy_editor();

        void begin_frame();

        void draw_editor();

        void end_frame();
    };
}

#endif
