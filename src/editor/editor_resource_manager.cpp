#include "editor_resource_manager.hpp"
#include <core/resource_manager.hpp>
#include <core/texture.hpp>
#include "editor_error_popup.hpp"
#include "editor_root.hpp"
#include "editor_entity_editor.hpp"
#include "editor_scene_editor.hpp"

#include <imgui.h>
#include <imgui_stdlib.h>

#include <array>
#include <stdio.h> 
#ifdef _WIN32
    #include <direct.h>
    #define GETCURRENTDIRECTORY _getcwd
#else
    #include <unistd.h>
    #define GETCURRENTDIRECTORY getcwd
#endif

namespace ivn::editor
{
    editor_delete_resource_warning::editor_delete_resource_warning(resource* res) : m_resource{res}
    {
        ebus::get_singleton().notify<close_delete_warning_event>({});

        m_collector.listen<close_delete_warning_event>([this](auto&)
        {
            m_active = false;
        });
    }

    void editor_delete_resource_warning::draw()
    {
        if (ImGui::Begin("Delete Resource", &m_active))
        {
            ImGui::TextWrapped("Warning: Are you sure you want to delete %s?", 
                m_resource->get_name().c_str());

            if (ImGui::Button("Yes"))
            {
                ebus::get_singleton().notify<resource_remove_event>({m_resource});
                m_active = false;
            }

            ImGui::SameLine();

            if (ImGui::Button("No"))
            {
                m_active = false;
            }
        }

        ImGui::End();
    }

    editor_rename_resource::editor_rename_resource(resource* res) : m_resource{res}
    {
        ebus::get_singleton().notify<close_rename_window_event>({});

        m_collector.listen<close_rename_window_event>([this](auto&)
        {
            m_active = false;
        });
    }

    void editor_rename_resource::draw()
    {
        if (ImGui::Begin("Rename Resource", &m_active))
        {
            ImGui::TextWrapped("Old name: %s", m_resource->get_name().c_str());
            ImGui::InputText("New name", &m_new_name);

            if (ImGui::Button("Save") && m_resource->get_name() != m_new_name)
            {
                ebus::get_singleton().notify<resource_rename_event>({m_resource, m_new_name});
                m_active = false;
            }
        }

        ImGui::End();
    }

    editor_resource_manager::editor_resource_manager()
    {
        std::array<char, FILENAME_MAX> buffer;
        GETCURRENTDIRECTORY(buffer.data(), FILENAME_MAX);
        m_resource_path = buffer.data();
    }

    editor_resource_manager::~editor_resource_manager()
    {
        ebus::get_singleton().notify<erm_closed_event>({});
    }
    
    void editor_resource_manager::draw()
    {
        if (ImGui::Begin("Resource Manager", &m_active))
        {
            draw_load_resource();

            ImGui::Separator();

            draw_resource_list();
        }

        ImGui::End();

        draw_children();
    }

    void editor_resource_manager::draw_load_resource()
    {
        if (ImGui::CollapsingHeader("Load Resource", ImGuiTreeNodeFlags_DefaultOpen))
        {
            ImGui::InputText("Resource Path", &m_resource_path);
            ImGui::InputText("Resource Name", &m_resource_name);

            if (ImGui::Button("Load") && !m_resource_path.empty() && 
                !m_resource_name.empty())
            {
                create_resource();
            }
        }
    }

    void editor_resource_manager::draw_resource_list()
    {
        ImGui::InputText("Filter", &m_find_res_name);

        resource_table* table;
        ebus::get_singleton().notify<resource_manager_get_all_resources>({table});

        std::int32_t i = 0;
        
        for (const auto& [name, res] : *table)
        {
            if (name.find(m_find_res_name) == std::string::npos) continue;

            ImGui::PushID(ImGui::GetID(this + i++));

            if (ImGui::TreeNode(name.c_str()))
            {
                switch (res->get_type())
                {
                    case resource_type::TEXTURE:
                    {
                        const texture* tex = static_cast<const texture*>(res.get());
                        ImTextureID id = reinterpret_cast<void*>(tex->get_id());
                        
                        ImGui::Image(id, ImVec2{128, 128}, ImVec2{0, 1}, ImVec2{1, 0});

                        ImGui::Separator();

                        std::string width = std::string{"Width: "} + std::to_string(tex->get_width());
                        std::string height = std::string{"Height: "} + std::to_string(tex->get_height());

                        ImGui::TextWrapped("%s", width.c_str());
                        ImGui::TextWrapped("%s", height.c_str());

                        ImGui::Separator();

                        if (ImGui::Button("Delete"))
                        {
                            add_component(std::make_unique<editor_delete_resource_warning>(res.get()));
                        }

                        break;
                    }

                    case resource_type::SCENE:
                    {
                        if (ImGui::Button("Delete"))
                        {
                            add_component(std::make_unique<editor_delete_resource_warning>(res.get()));
                        }

                        ImGui::SameLine();

                        if (ImGui::Button("Edit"))
                        {
                            ebus::get_singleton().notify<open_scene_editor_event>({});
                            ebus::get_singleton().notify<set_scene_filter_event>({name});
                        }

                        ImGui::SameLine();

                        break;
                    }

                    case resource_type::ENTITY:
                    {
                        if (ImGui::Button("Delete"))
                        {
                            add_component(std::make_unique<editor_delete_resource_warning>(res.get()));
                        }

                        ImGui::SameLine();

                        if (ImGui::Button("Edit"))
                        {
                            ebus::get_singleton().notify<open_entity_editor_event>({});
                            ebus::get_singleton().notify<set_entity_filter_event>({name});
                        }

                        ImGui::SameLine();

                        break;
                    }

                    case resource_type::MESH:
                    case resource_type::SHADER:
                    {
                        ImGui::TreePop();
                        ImGui::PopID();
                        continue;
                    }

                    default:
                        break;
                }

                if (ImGui::Button("Rename"))
                {
                    add_component(std::make_unique<editor_rename_resource>(res.get()));
                }

                ImGui::TreePop();
            }

            ImGui::PopID();
        }
    }

    void editor_resource_manager::create_resource()
    {
        try
        {
            auto ext = m_resource_path.substr(m_resource_path.find_last_of(".") + 1);

            std::unique_ptr<resource> res;

            if (ext == "png" || ext == "jpg" || ext == "jpeg")
            {
                res = std::make_unique<texture>(m_resource_name, m_resource_path);
            }
            else
            {
                throw std::runtime_error{"Invalid resource type: " + ext};
            }

            if (res)
            {
                ebus::get_singleton().notify<resource_add_event>({std::move(res)});
            }
        }
        catch (const std::exception& e)
        {
            add_component(std::make_unique<editor_error_popup>(e.what()));
        }
    }
}
