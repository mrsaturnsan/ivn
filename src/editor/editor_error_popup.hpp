#ifndef EDITOR_ERROR_POPUP_HPP
#define EDITOR_ERROR_POPUP_HPP

#include "editor.hpp"

namespace ivn::editor
{
    struct close_error_popup_window_event {};

    class editor_error_popup : public editor_component
    {
    private:
        std::string m_error_message;

    public:
        editor_error_popup(const std::string& error_message);

    protected:
        void draw() override;
    };
}

#endif
