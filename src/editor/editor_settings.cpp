#include "editor_settings.hpp"
#include <core/renderer.hpp>
#include <core/scene_manager.hpp>
#include <core/resource_manager.hpp>

#include <imgui.h>
#include <imgui_stdlib.h>

#include <vector>

namespace ivn::editor
{
    editor_settings::~editor_settings()
    {
        ebus::get_singleton().notify<settings_closed_event>({});
    }

    void editor_settings::draw()
    {
        if (ImGui::Begin("Settings", &m_active))
        {
            if (ImGui::CollapsingHeader("Project Settings"))
            {
                ImGui::InputText("Game Name", &m_game_name);
                ImGui::InputText("Creator", &m_creator_name);
            }

            if (ImGui::CollapsingHeader("Game Settings"))
            {
                if (ImGui::ColorEdit4("Background Color", m_clear_color.data()))
                {
                    ebus::get_singleton().notify<set_clear_color_event>({m_clear_color});
                }

                ImGui::Checkbox("Show ImGui Demo Window?", &m_show_demo_window);

                if (m_show_demo_window)
                {
                    ImGui::ShowDemoWindow(&m_show_demo_window);
                }
            }
        }

        ImGui::End();
    }
}
