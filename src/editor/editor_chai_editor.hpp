#ifndef EDITOR_CHAI_EDITOR_HPP
#define EDITOR_CHAI_EDITOR_HPP

#include "editor.hpp"

namespace ivn::editor
{
    struct chai_editor_closed_event {};

    class chai_editor : public editor_component
    {
    private:
        std::string m_script_name;
        std::string m_find_script_name;

    public:
        ~chai_editor() override;

    protected:
        void draw() override;

        void draw_bound_entity_table();

        void draw_create_script();

        void draw_script_tree();
    };

}

#endif
