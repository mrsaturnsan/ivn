add_library(imgui
    STATIC
        imgui.cpp
        imgui_draw.cpp
        imgui_widgets.cpp
        imgui_demo.cpp
        imgui_impl_glfw.cpp
        imgui_impl_opengl3.cpp
        imgui_stdlib.cpp
)

find_package(GLEW REQUIRED)
find_package(glfw3 REQUIRED)

target_include_directories(imgui
    PUBLIC
        ${CMAKE_CURRENT_SOURCE_DIR}
    PRIVATE
        ${GLEW_INCLUDE_DIRS}
)

target_link_libraries(imgui
    PRIVATE
        ${GLEW_LIBRARIES}
        glfw
)

target_compile_definitions(imgui 
    PRIVATE
        IMGUI_IMPL_OPENGL_LOADER_GLEW
)