#pragma once

#include <algorithm>
#include <functional>
#include <vector>
#include <variant>

#include "CallbackVector.h"

namespace Dexode
{
namespace Internal
{

template <typename... Base>
struct visitor : Base...
{
	using Base::operator()...;
};

template <typename... T> visitor(T...) -> visitor<T...>;

template <typename Event>
struct TransactionCallbackVector : public CallbackVector
{
	using CallbackType = std::function<void(const Event&)>;
	using ContainerElement = std::pair<int, CallbackType>;
	using ContainerType = std::vector<ContainerElement>;
	ContainerType container;
	
	std::vector<std::variant<ContainerElement, int>> toHandle;
	int inTransaction = 0;

	virtual void remove(const int token) override
	{
		auto removeFrom = std::find_if(
			container.begin(), container.end(), [token](const ContainerElement& element) {
				return element.first == token;
			});

		if(removeFrom == container.end())
		{
			return;
		}
			
		if(inTransaction > 0)
		{
			removeFrom->second = [](auto&) {};
			toHandle.emplace_back(token);
		}
		else
		{
			container.erase(removeFrom, container.end());
		}
	}

	void add(const int token, const CallbackType& callback)
	{
		if(inTransaction > 0)
		{
			toHandle.emplace_back(std::in_place_type<ContainerElement>, token, callback);
		}
		else
		{
			container.emplace_back(token, callback);
		}
	}

	void beginTransaction()
	{
		++inTransaction;
	}

	void commitTransaction()
	{
		--inTransaction;
		if(inTransaction > 0)
		{
			return;
		}
		inTransaction = 0;

		if(toHandle.empty() == false)
		{
			container.reserve(toHandle.size());
			
			visitor vstr
			{
				[this](ContainerElement& elem)
				{
					container.emplace_back(elem);
				},
				[this](int i)
				{
					remove(i);
				}
			};

			for(auto& var : toHandle)
			{
				std::visit(vstr, var);
			}

			toHandle.clear();
		}
	}
};

} // namespace Internal
} // namespace Dexode
